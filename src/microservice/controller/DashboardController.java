/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package microservice.controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import microservice.service.DoughnutChart;

/**
 * FXML Controller class
 *
 * @author Dell
 */
public class DashboardController implements Initializable {

    @FXML
    private Label provLabel;

    @FXML
    private Label munLabel;

    @FXML
    private Label wardLabel;

    @FXML
    private Label marghLabel;

    @FXML
    private AnchorPane piePane;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        loadPosterData();
        loadPieChart();
    }

    private void loadPosterData() {
        int totalProv = 7;
        int totalMun = 753;
        int totalWard = 3608;
        int totalMargh = 56432;
        provLabel.setText(String.valueOf(totalProv));
        munLabel.setText(String.valueOf(totalMun));
        wardLabel.setText(String.valueOf(totalWard));
        marghLabel.setText(String.valueOf(totalMargh));
    }

    private void loadPieChart() {
        ObservableList<PieChart.Data> pieData =
                FXCollections.observableArrayList(
                        new PieChart.Data("Kathmandu", 20),
                        new PieChart.Data("Pokhara", 25),
                        new PieChart.Data("Bhaktapur", 10),
                        new PieChart.Data("Bharatpur", 15),
                        new PieChart.Data("Nepalgunj", 15),
                        new PieChart.Data("Biratnagar", 15));

        final DoughnutChart chart = new DoughnutChart(pieData);
        chart.setTitle("Total Margh per Municipality");
        piePane.getChildren().add(chart); 
    }
}
