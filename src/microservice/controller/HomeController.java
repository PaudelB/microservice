/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package microservice.controller;

/**
 *
 * @author Bimal Paudel
 */
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;

public class HomeController {

    @FXML
    private Pane contentPane;

    public void initialize(URL url, ResourceBundle rb) {
        try {
            Pane newLoadedPane = FXMLLoader.load(getClass().getResource("/microservice/view/Dashboard.fxml"));
            contentPane.getChildren().add(newLoadedPane);
        } catch (IOException e) {
            System.out.print(e);
        }
    }

    @FXML
    private void dashboard(MouseEvent event) {
        try {
            contentPane.getChildren().clear();
            Pane newLoadedPane = FXMLLoader.load(getClass().getResource("/microservice/view/Dashboard.fxml"));
            contentPane.getChildren().add(newLoadedPane);
        } catch (IOException e) {
            System.out.print(e);
        }
    }

    @FXML
    private void province(MouseEvent event) {
        try {
            contentPane.getChildren().clear();

            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/microservice/view/province/Index.fxml"));
            Pane newLoadedPane = fxmlLoader.load();

            ProvinceController controller = (ProvinceController) fxmlLoader.getController();
            controller.paneSetting();
            contentPane.getChildren().add(newLoadedPane);
        } catch (IOException e) {
            System.out.print(e);
        }
    }

    @FXML
    private void municipality(MouseEvent event) {
        try {
            contentPane.getChildren().clear();

            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/microservice/view/municipality/Index.fxml"));
            Pane newLoadedPane = fxmlLoader.load();

            MunicipalityController controller = (MunicipalityController) fxmlLoader.getController();
            controller.paneSetting();
            contentPane.getChildren().add(newLoadedPane);
        } catch (IOException e) {
            System.out.print(e);
        }
    }

    @FXML
    private void ward(MouseEvent event) {
        try {
            contentPane.getChildren().clear();

            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/microservice/view/ward/Index.fxml"));
            Pane newLoadedPane = fxmlLoader.load();

            WardController controller = (WardController) fxmlLoader.getController();
            controller.paneSetting();
            contentPane.getChildren().add(newLoadedPane);
        } catch (IOException e) {
            System.out.print(e);
        }
    }

    @FXML
    private void tole(MouseEvent event) {
        try {
            contentPane.getChildren().clear();

            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/microservice/view/tole/Index.fxml"));
            Pane newLoadedPane = fxmlLoader.load();

            ToleController controller = (ToleController) fxmlLoader.getController();
            controller.paneSetting();
            contentPane.getChildren().add(newLoadedPane);
        } catch (IOException e) {
            System.out.print(e);
        }
    }

    @FXML
    private void margh(MouseEvent event) {
        try {
            contentPane.getChildren().clear();

            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/microservice/view/margh/Index.fxml"));
            Pane newLoadedPane = fxmlLoader.load();

            MarghController controller = (MarghController) fxmlLoader.getController();
            controller.paneSetting();
            contentPane.getChildren().add(newLoadedPane);
        } catch (IOException e) {
            System.out.print(e);
        }
    }
}
