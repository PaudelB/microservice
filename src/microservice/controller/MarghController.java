/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package microservice.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.validation.RequiredFieldValidator;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import java.awt.HeadlessException;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;
import javafx.util.Duration;
import javafx.util.StringConverter;
import org.controlsfx.control.Notifications;
import microservice.model.Margh;
import microservice.model.Municipality;
import microservice.model.Province;
import microservice.service.Service;
import microservice.model.Tole;
import microservice.model.Ward;

/**
 * FXML Controller class
 *
 * @author Dell
 */
public class MarghController implements Initializable {

    private final MarghController mhc = this;

    private int cProvId;

    private int cMunId;

    private int cWardId;

    private int cToleId;

    private int marghId;

    private int cMarghId;

    @FXML
    private AnchorPane contentPane;
    @FXML
    private StackPane marghStackPane;
    @FXML
    private Pane firstPane;
    @FXML
    private Pane secondPane;

    @FXML
    private JFXTextField cMargh;

    @FXML
    private JFXTextArea cDesc;

    @FXML
    private JFXComboBox<Province> cProv;

    @FXML
    private JFXComboBox<Municipality> cMunicipality;

    @FXML
    private JFXComboBox<Ward> cWard;

    @FXML
    private JFXComboBox<Tole> cTole;

    @FXML
    private TableView<Margh> tbMarghList;

    @FXML
    private TableColumn<Margh, Number> tbId;

    @FXML
    private TableColumn<Margh, Number> tbWard;

    @FXML
    private TableColumn<Margh, String> tbTole;

    @FXML
    private TableColumn<Margh, String> tbMargh;

    @FXML
    private TableColumn<Margh, Void> tbAction;

    @FXML
    private Label updateTitle;

    @FXML
    private Label vCreatedAt;

    @FXML
    private Label vDesc;

    @FXML
    private Label vProvince;

    @FXML
    private Label vStatus;

    @FXML
    private Label vMunicipality;

    @FXML
    private Label vWard;

    @FXML
    private Label vTole;

    @FXML
    private Label vMargh;

    @FXML
    private JFXComboBox<Margh> aMargh;

    @FXML
    private TextField wardSearchBox;
       
    @FXML
    private TextField toleSearchBox;
    
    @FXML
    private TextField marghSearchBox;

    @FXML
    void doneView(MouseEvent event) {
        try {
            firstPane.getChildren().clear();
            FXMLLoader fFxmlLoader = new FXMLLoader(getClass().getResource("/microservice/view/margh/Create.fxml"));
            fFxmlLoader.setController(mhc);
            Pane fNewLoadedPane = fFxmlLoader.load();
            firstPane.getChildren().add(fNewLoadedPane);

            loadProvData();
        } catch (IOException ie) {
            System.out.print(ie);
            Logger.getLogger(MarghController.class.getName()).log(Level.SEVERE, null, ie);
            ie.printStackTrace();
        }
    }

    @FXML
    void clear(MouseEvent event) {
        clearCreate();
    }

    public void clearCreate() {
        cProv.setValue(null);
        cMunicipality.setValue(null);
        cWard.setValue(null);
        cTole.setValue(null);
        cMargh.clear();
        cDesc.clear();
    }

    @FXML
    void create(MouseEvent event) {
        String addMargh = "INSERT INTO MARGH (TOLE_ID, MARGH, DESCRIPTION, STATUS, CREATED_AT) VALUES (?, ?, ?, ?, ?)";

        RequiredFieldValidator empValid = new RequiredFieldValidator();
        empValid.setMessage("Field value required.");

        cProv.setValidators(empValid);
        cMunicipality.setValidators(empValid);
        cWard.setValidators(empValid);
        cTole.setValidators(empValid);
        cMargh.setValidators(empValid);

        if (cProv.validate() && cMunicipality.validate() && cWard.validate() && cTole.validate() && cMargh.validate()) {
            try {
                Timestamp createdAt = new Timestamp(System.currentTimeMillis());

                try (Connection con = Service.ConnectDB()) {
                    PreparedStatement statement = con.prepareStatement(addMargh, Statement.RETURN_GENERATED_KEYS);
                    statement.setInt(1, cToleId);
                    statement.setString(2, cMargh.getText());
                    statement.setString(3, cDesc.getText());
                    statement.setBoolean(4, true);
                    statement.setTimestamp(5, createdAt);

                    statement.executeUpdate();
                    ResultSet rs = statement.getGeneratedKeys();

                    if (rs.next()) {
                        int generatedKey = rs.getInt(1);
                        FontAwesomeIconView tick = new FontAwesomeIconView(FontAwesomeIcon.CHECK_CIRCLE);
                        tick.setStyle("-fx-fill: LIGHTGREEN");
                        tick.setGlyphSize(50);
                        Notifications notify = Notifications.create()
                                .title("Success")
                                .text("New margh successfully added.")
                                .graphic(tick)
                                .hideAfter(Duration.seconds(5))
                                .position(Pos.TOP_RIGHT);
                        notify.darkStyle();
                        notify.show();

//                        tbMarghList.getItems().clear();
                        tbMarghList.setItems(null);
                        loadMarghData();

                        clearCreate();

                        firstPane.getChildren().clear();
                        FXMLLoader fFxmlLoader = new FXMLLoader(getClass().getResource("/microservice/view/margh/View.fxml"));
                        fFxmlLoader.setController(mhc);
                        Pane fNewLoadedPane = fFxmlLoader.load();
                        firstPane.getChildren().add(fNewLoadedPane);

                        loadViewMargh(generatedKey);

                    } else {
                        FontAwesomeIconView tick = new FontAwesomeIconView(FontAwesomeIcon.TIMES_CIRCLE);
                        tick.setStyle("-fx-fill: ORANGERED");
                        tick.setGlyphSize(50);
                        Notifications notify = Notifications.create()
                                .title("Failed")
                                .text("Error occured while adding margh!")
                                .graphic(tick)
                                .hideAfter(Duration.seconds(5))
                                .position(Pos.TOP_RIGHT);
                        notify.darkStyle();
                        notify.show();
                    }

                    con.commit();
                    con.close();
                }
            } catch (HeadlessException | SQLException | IOException ex) {
                FontAwesomeIconView tick = new FontAwesomeIconView(FontAwesomeIcon.TIMES_CIRCLE);
                tick.setStyle("-fx-fill: ORANGERED");
                tick.setGlyphSize(50);
                Notifications notify = Notifications.create()
                        .title("Failed")
                        .text("Error occured while adding margh!")
                        .graphic(tick)
                        .hideAfter(Duration.seconds(5))
                        .position(Pos.TOP_RIGHT);
                notify.darkStyle();
                notify.show();

                System.out.print(ex);
                Logger.getLogger(MarghController.class.getName()).log(Level.SEVERE, null, ex);
                ex.printStackTrace();
            }
        }
    }

    @FXML
    void cancel(MouseEvent event) {
        try {
            firstPane.getChildren().clear();
            FXMLLoader fFxmlLoader = new FXMLLoader(getClass().getResource("/microservice/view/margh/Create.fxml"));
            fFxmlLoader.setController(mhc);
            Pane fNewLoadedPane = fFxmlLoader.load();
            firstPane.getChildren().add(fNewLoadedPane);

            loadProvData();

        } catch (IOException ie) {
            System.out.print(ie);
            Logger.getLogger(MarghController.class.getName()).log(Level.SEVERE, null, ie);
            ie.printStackTrace();
        }
    }

    @FXML
    void update(MouseEvent event) {
        String updateWard = "UPDATE MARGH SET TOLE_ID = ?, MARGH = ?, DESCRIPTION = ?, CREATED_AT = ? WHERE ID = ?";

        RequiredFieldValidator empValid = new RequiredFieldValidator();
        empValid.setMessage("Field value required.");

        cProv.setValidators(empValid);
        cMunicipality.setValidators(empValid);
        cWard.setValidators(empValid);
        cTole.setValidators(empValid);
        cMargh.setValidators(empValid);

        if (cProv.validate() && cMunicipality.validate() && cWard.validate() && cTole.validate() && cMargh.validate()) {
            try {
                Timestamp createdAt = new Timestamp(System.currentTimeMillis());
                int rowsUpdated;
                try (Connection con = Service.ConnectDB()) {

                    PreparedStatement statement = con.prepareStatement(updateWard);
                    statement.setInt(1, cToleId);
                    statement.setString(2, cMargh.getText());
                    statement.setString(3, cDesc.getText());
                    statement.setTimestamp(4, createdAt);

                    statement.setLong(5, marghId);

                    rowsUpdated = statement.executeUpdate();

                    if (rowsUpdated > 0) {
                        FontAwesomeIconView tick = new FontAwesomeIconView(FontAwesomeIcon.CHECK_CIRCLE);
                        tick.setStyle("-fx-fill: LIGHTGREEN");
                        tick.setGlyphSize(50);
                        Notifications notify = Notifications.create()
                                .title("Success")
                                .text("Margh updated successfully.")
                                .graphic(tick)
                                .hideAfter(Duration.seconds(5))
                                .position(Pos.TOP_RIGHT);
                        notify.darkStyle();
                        notify.show();

//                        tbMarghList.getItems().clear();
                        tbMarghList.setItems(null);
                        loadMarghData();

                        clearCreate();

                        firstPane.getChildren().clear();
                        FXMLLoader fFxmlLoader = new FXMLLoader(getClass().getResource("/microservice/view/margh/View.fxml"));
                        fFxmlLoader.setController(mhc);
                        Pane fNewLoadedPane = fFxmlLoader.load();
                        firstPane.getChildren().add(fNewLoadedPane);

                        loadViewMargh(marghId);

                    } else {
                        FontAwesomeIconView tick = new FontAwesomeIconView(FontAwesomeIcon.TIMES_CIRCLE);
                        tick.setStyle("-fx-fill: ORANGERED");
                        tick.setGlyphSize(50);
                        Notifications notify = Notifications.create()
                                .title("Failed")
                                .text("Error occured while updating margh!")
                                .graphic(tick)
                                .hideAfter(Duration.seconds(5))
                                .position(Pos.TOP_RIGHT);
                        notify.darkStyle();
                        notify.show();
                    }

                    con.commit();
                    con.close();
                }

            } catch (HeadlessException | SQLException | IOException ex) {
                FontAwesomeIconView tick = new FontAwesomeIconView(FontAwesomeIcon.TIMES_CIRCLE);
                tick.setStyle("-fx-fill: ORANGERED");
                tick.setGlyphSize(50);
                Notifications notify = Notifications.create()
                        .title("Failed")
                        .text("Error occured while updating margh!")
                        .graphic(tick)
                        .hideAfter(Duration.seconds(5))
                        .position(Pos.TOP_RIGHT);
                notify.darkStyle();
                notify.show();

                System.out.print(ex);
                Logger.getLogger(MarghController.class.getName()).log(Level.SEVERE, null, ex);
                ex.printStackTrace();
            }
        }
    }

    public void paneSetting() {
        try {
            firstPane.getChildren().clear();
            FXMLLoader fFxmlLoader = new FXMLLoader(getClass().getResource("/microservice/view/margh/Create.fxml"));
            fFxmlLoader.setController(mhc);
            Pane fNewLoadedPane = fFxmlLoader.load();
            firstPane.getChildren().add(fNewLoadedPane);

            loadProvData();

            secondPane.getChildren().clear();
            FXMLLoader sFxmlLoader = new FXMLLoader(getClass().getResource("/microservice/view/margh/Data.fxml"));
            sFxmlLoader.setController(mhc);
            Pane sNewLoadedPane = sFxmlLoader.load();
            secondPane.getChildren().add(sNewLoadedPane);

            loadMarghData();

        } catch (IOException ie) {
            System.out.print(ie);
            Logger.getLogger(MarghController.class.getName()).log(Level.SEVERE, null, ie);
            ie.printStackTrace();
        }
    }

    public void loadProvData() {
        cProv.setConverter(new StringConverter<Province>() {
            @Override
            public String toString(Province object) {
                return object.getProvince();
            }

            @Override
            public Province fromString(String string) {
                return null;
            }
        });

        ObservableList<Province> provData = FXCollections.observableArrayList();
        try {
            Connection con = Service.ConnectDB();

            String allProv = "SELECT * FROM PROVINCE WHERE STATUS = ?";

            PreparedStatement pst = con.prepareStatement(allProv);
            pst.setBoolean(1, true);

            ResultSet rs = pst.executeQuery();

            while (rs.next()) {
                provData.add(new Province(rs.getInt("ID"), rs.getString("PROVINCE"), rs.getString("DESCRIPTION"), rs.getBoolean("STATUS"), rs.getDate("CREATED_AT")));
            }
            cProv.getItems().addAll(provData);

        } catch (SQLException se) {
            System.out.println(se);
            Logger.getLogger(MarghController.class.getName()).log(Level.SEVERE, null, se);
            se.printStackTrace();

        }

        cProv.valueProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                cProvId = newValue.getId();
                cMunicipality.getItems().clear();
                cWard.getItems().clear();
                cTole.getItems().clear();
                if(aMargh != null) {
                    aMargh.getItems().clear();
                }
                loadMunData();
            }
        });
    }

    public void loadMunData() {
        cMunicipality.setConverter(new StringConverter<Municipality>() {
            @Override
            public String toString(Municipality object) {
                return object.getMunicipality();
            }

            @Override
            public Municipality fromString(String string) {
                return null;
            }
        });

        ObservableList<Municipality> munData = FXCollections.observableArrayList();
        try {
            Connection con = Service.ConnectDB();

            String allMun = "SELECT MUNICIPALITY.ID, MUNICIPALITY.PROV_ID, PROVINCE.PROVINCE, MUNICIPALITY.MUNICIPALITY, MUNICIPALITY.DESCRIPTION, MUNICIPALITY.STATUS, MUNICIPALITY.CREATED_AT FROM MUNICIPALITY INNER JOIN PROVINCE ON PROVINCE.ID = MUNICIPALITY.PROV_ID WHERE MUNICIPALITY.PROV_ID = ? AND MUNICIPALITY.STATUS = ?";

            PreparedStatement pst = con.prepareStatement(allMun);
            pst.setInt(1, cProvId);
            pst.setBoolean(2, true);

            ResultSet rs = pst.executeQuery();

            while (rs.next()) {
                munData.add(new Municipality(rs.getInt("ID"), rs.getInt("PROV_ID"), rs.getString("PROVINCE"), rs.getString("MUNICIPALITY"), rs.getString("DESCRIPTION"), rs.getBoolean("STATUS"), rs.getDate("CREATED_AT")));
            }
            cMunicipality.getItems().addAll(munData);

        } catch (SQLException se) {
            System.out.println(se);
            Logger.getLogger(MarghController.class.getName()).log(Level.SEVERE, null, se);
            se.printStackTrace();

        }

        cMunicipality.valueProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                cMunId = newValue.getId();
                cWard.getItems().clear();
                cTole.getItems().clear();
                if(aMargh != null) {
                    aMargh.getItems().clear();
                }
                loadWardData();
            }
        });
    }

    public void loadWardData() {
        cWard.setConverter(new StringConverter<Ward>() {
            @Override
            public String toString(Ward object) {
                return String.valueOf(object.getWard());
            }

            @Override
            public Ward fromString(String string) {
                return null;
            }
        });

        ObservableList<Ward> wardData = FXCollections.observableArrayList();
        try {
            Connection con = Service.ConnectDB();

            String allWard = "SELECT WARD.ID, WARD.MUN_ID, MUNICIPALITY.MUNICIPALITY, PROVINCE.PROVINCE, WARD.WARD, WARD.DESCRIPTION, WARD.STATUS, WARD.CREATED_AT FROM WARD INNER JOIN MUNICIPALITY ON MUNICIPALITY.ID = WARD.MUN_ID INNER JOIN PROVINCE ON PROVINCE.ID = MUNICIPALITY.PROV_ID WHERE WARD.MUN_ID = ? AND WARD.STATUS = ?";

            PreparedStatement pst = con.prepareStatement(allWard);
            pst.setInt(1, cMunId);
            pst.setBoolean(2, true);

            ResultSet rs = pst.executeQuery();

            while (rs.next()) {
                wardData.add(new Ward(rs.getInt("ID"), rs.getInt("MUN_ID"), rs.getString("PROVINCE"), rs.getString("MUNICIPALITY"), rs.getInt("WARD"), rs.getString("DESCRIPTION"), rs.getBoolean("STATUS"), rs.getDate("CREATED_AT")));
            }

            cWard.getItems().addAll(wardData);

        } catch (SQLException se) {
            System.out.println(se);
            Logger.getLogger(MarghController.class.getName()).log(Level.SEVERE, null, se);
            se.printStackTrace();

        }

        cWard.valueProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                cWardId = newValue.getId();
                cTole.getItems().clear();
                if(aMargh != null) {
                    aMargh.getItems().clear();
                }
                loadToleData();
            }
        });
    }

    public void loadToleData() {
        cTole.setConverter(new StringConverter<Tole>() {
            @Override
            public String toString(Tole object) {
                return object.getTole();
            }

            @Override
            public Tole fromString(String string) {
                return null;
            }
        });

        ObservableList<Tole> toleData = FXCollections.observableArrayList();
        try {
            Connection con = Service.ConnectDB();

            String allTole = "SELECT TOLE.ID, TOLE.WARD_ID, PROVINCE.PROVINCE, MUNICIPALITY.MUNICIPALITY, WARD.WARD, TOLE.TOLE, TOLE.DESCRIPTION, TOLE.STATUS, TOLE.CREATED_AT FROM TOLE INNER JOIN WARD ON WARD.ID = TOLE.WARD_ID INNER JOIN MUNICIPALITY ON MUNICIPALITY.ID = WARD.MUN_ID INNER JOIN PROVINCE ON PROVINCE.ID = MUNICIPALITY.PROV_ID WHERE TOLE.WARD_ID = ? AND TOLE.STATUS = ?";

            PreparedStatement pst = con.prepareStatement(allTole);
            pst.setInt(1, cWardId);
            pst.setBoolean(2, true);

            ResultSet rs = pst.executeQuery();

            while (rs.next()) {
                toleData.add(new Tole(rs.getInt("ID"), rs.getInt("WARD_ID"), rs.getString("PROVINCE"), rs.getString("MUNICIPALITY"), rs.getInt("WARD"), rs.getString("TOLE"), rs.getString("DESCRIPTION"), rs.getBoolean("STATUS"), rs.getDate("CREATED_AT")));
            }

            cTole.getItems().addAll(toleData);

        } catch (SQLException se) {
            System.out.println(se);
            Logger.getLogger(MarghController.class.getName()).log(Level.SEVERE, null, se);
            se.printStackTrace();

        }

        cTole.valueProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                cToleId = newValue.getId();
                if(aMargh != null) {
                    aMargh.getItems().clear();
                    loadAssMarghData();
                }
            }
        });
    }

    public void loadAssMarghData() {
        aMargh.setConverter(new StringConverter<Margh>() {
            @Override
            public String toString(Margh object) {
                return object.getMargh();
            }

            @Override
            public Margh fromString(String string) {
                return null;
            }
        });

        ObservableList<Margh> marghData = FXCollections.observableArrayList();
        try {
            Connection con = Service.ConnectDB();

            String allMargh = "SELECT MARGH.ID, MARGH.TOLE_ID, PROVINCE.PROVINCE, MUNICIPALITY.MUNICIPALITY, WARD.WARD, TOLE.TOLE, MARGH.MARGH, MARGH.DESCRIPTION, MARGH.STATUS, MARGH.CREATED_AT FROM MARGH INNER JOIN TOLE ON TOLE.ID = MARGH.TOLE_ID INNER JOIN WARD ON WARD.ID = TOLE.WARD_ID INNER JOIN MUNICIPALITY ON MUNICIPALITY.ID = WARD.MUN_ID INNER JOIN PROVINCE ON PROVINCE.ID = MUNICIPALITY.PROV_ID WHERE MARGH.TOLE_ID = ? AND MARGH.STATUS = ? ORDER BY LOWER(MARGH) ASC";

            PreparedStatement pst = con.prepareStatement(allMargh);
            pst.setInt(1, cToleId);
            pst.setBoolean(2, true);

            ResultSet rs = pst.executeQuery();

            while (rs.next()) {
                marghData.add(new Margh(rs.getInt("ID"), rs.getInt("TOLE_ID"), rs.getString("PROVINCE"), rs.getString("MUNICIPALITY"), rs.getInt("WARD"), rs.getString("TOLE"), rs.getString("MARGH"), rs.getString("DESCRIPTION"), rs.getBoolean("STATUS"), rs.getDate("CREATED_AT")));
            }

            aMargh.getItems().addAll(marghData);

        } catch (SQLException se) {
            System.out.println(se);
            Logger.getLogger(MarghController.class.getName()).log(Level.SEVERE, null, se);
            se.printStackTrace();

        }

        aMargh.valueProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                cMarghId = newValue.getId();
            }
        });
    }

    public void loadMarghData() {
        ObservableList<Margh> marghData = FXCollections.observableArrayList();
        try {
            Connection con = Service.ConnectDB();

            String allMargh = "SELECT MARGH.ID, MARGH.TOLE_ID, PROVINCE.PROVINCE, MUNICIPALITY.MUNICIPALITY, WARD.WARD, TOLE.TOLE, MARGH.MARGH, MARGH.DESCRIPTION, MARGH.STATUS, MARGH.CREATED_AT FROM MARGH INNER JOIN TOLE ON TOLE.ID = MARGH.TOLE_ID INNER JOIN WARD ON WARD.ID = TOLE.WARD_ID INNER JOIN MUNICIPALITY ON MUNICIPALITY.ID = WARD.MUN_ID INNER JOIN PROVINCE ON PROVINCE.ID = MUNICIPALITY.PROV_ID WHERE MARGH.STATUS = ?  ORDER BY LOWER(MARGH) ASC";

            PreparedStatement pst = con.prepareStatement(allMargh);
            pst.setBoolean(1, true);

            ResultSet rs = pst.executeQuery();

            while (rs.next()) {
                marghData.add(new Margh(rs.getInt("ID"), rs.getInt("TOLE_ID"), rs.getString("PROVINCE"), rs.getString("MUNICIPALITY"), rs.getInt("WARD"), rs.getString("TOLE"), rs.getString("MARGH"), rs.getString("DESCRIPTION"), rs.getBoolean("STATUS"), rs.getDate("CREATED_AT")));
            }
            FilteredList<Margh> filteredData = new FilteredList<>(marghData, p -> true);
            
            wardSearchBox.textProperty().addListener((observable, oldValue, newValue) -> {
                filteredData.setPredicate(margh -> {
                    if (newValue == null || newValue.isEmpty()) {
                        return true;
                    }

                    String lowerCaseFilter = newValue.toLowerCase();

                    if (String.valueOf(margh.getWard()).toLowerCase().indexOf(lowerCaseFilter) != -1) {
                        return true;
                    }
                    return false;
                });
            });
            toleSearchBox.textProperty().addListener((observable, oldValue, newValue) -> {
                filteredData.setPredicate(margh -> {
                    if (newValue == null || newValue.isEmpty()) {
                        return true;
                    }

                    String lowerCaseFilter = newValue.toLowerCase();

                    if (margh.getTole().toLowerCase().indexOf(lowerCaseFilter) != -1) {
                        return true;
                    }
                    return false;
                });
            });
            marghSearchBox.textProperty().addListener((observable, oldValue, newValue) -> {
                filteredData.setPredicate(margh -> {
                    if (newValue == null || newValue.isEmpty()) {
                        return true;
                    }
                    String lowerCaseFilter = newValue.toLowerCase();
                    if (margh.getMargh().toLowerCase().indexOf(lowerCaseFilter) != -1) {
                        return true;
                    }
                    return false;
                });
            });
            
            SortedList<Margh> sortedData = new SortedList<>(filteredData);
            sortedData.comparatorProperty().bind(tbMarghList.comparatorProperty());

            tbMarghList.setItems(sortedData);

//            tbMarghList.getItems().addAll(marghData);
        } catch (SQLException se) {
            System.out.println(se);
            Logger.getLogger(MarghController.class.getName()).log(Level.SEVERE, null, se);
            se.printStackTrace();

        }

        tbId.setSortable(false);
        tbId.setCellValueFactory(column -> new ReadOnlyObjectWrapper<Number>(tbMarghList.getItems().indexOf(column.getValue()) + 1));
        tbWard.setCellValueFactory(new PropertyValueFactory<>("ward"));
        tbTole.setCellValueFactory(new PropertyValueFactory<>("tole"));
        tbMargh.setCellValueFactory(new PropertyValueFactory<>("margh"));
        tbAction.setCellFactory(param -> new TableCell<Margh, Void>() {
            private final Button viewButton = new Button();
            private final Button editButton = new Button();
            private final Button deleteButton = new Button();
            private final HBox pane = new HBox(viewButton, editButton, deleteButton);

            {
                HBox.setMargin(editButton, new Insets(0, 0, 0, 2));
                HBox.setMargin(deleteButton, new Insets(0, 0, 0, 2));
                FontAwesomeIconView eye = new FontAwesomeIconView(FontAwesomeIcon.EYE);
                eye.setStyle("-fx-fill: DODGERBLUE");
                eye.setGlyphSize(12);

                FontAwesomeIconView edit = new FontAwesomeIconView(FontAwesomeIcon.EDIT);
                edit.setStyle("-fx-fill: DARKGREEN");
                edit.setGlyphSize(12);
                FontAwesomeIconView plus = new FontAwesomeIconView(FontAwesomeIcon.PLUS);
                plus.setStyle("-fx-fill: DARKORANGE");
                plus.setGlyphSize(11);
                FontAwesomeIconView delete = new FontAwesomeIconView(FontAwesomeIcon.TRASH);
                delete.setStyle("-fx-fill: ORANGERED");
                delete.setGlyphSize(12);

                viewButton.setGraphic(eye);
                viewButton.setTooltip(new Tooltip("View"));
                viewButton.getStyleClass().add("eye-btn");
                viewButton.setOnAction(event -> {
                    try {
                        Margh getMargh = getTableView().getItems().get(getIndex());

                        firstPane.getChildren().clear();
                        FXMLLoader fFxmlLoader = new FXMLLoader(getClass().getResource("/microservice/view/margh/View.fxml"));
                        fFxmlLoader.setController(mhc);
                        Pane fNewLoadedPane = fFxmlLoader.load();
                        firstPane.getChildren().add(fNewLoadedPane);

                        loadViewMargh(getMargh.getId());
                    } catch (IOException ex) {
                        System.out.println(ex);
                        Logger.getLogger(MarghController.class.getName()).log(Level.SEVERE, null, ex);
                        ex.printStackTrace();
                    }
                });

                editButton.setGraphic(edit);
                editButton.setTooltip(new Tooltip("Edit"));
                editButton.getStyleClass().add("edit-btn");
                editButton.setOnAction(event -> {

                    try {
                        Margh getMargh = getTableView().getItems().get(getIndex());

                        firstPane.getChildren().clear();
                        FXMLLoader fFxmlLoader = new FXMLLoader(getClass().getResource("/microservice/view/margh/Update.fxml"));
                        fFxmlLoader.setController(mhc);
                        Pane fNewLoadedPane = fFxmlLoader.load();
                        firstPane.getChildren().add(fNewLoadedPane);

                        loadUpdateMargh(getMargh.getId());
                        marghId = getMargh.getId();
                    } catch (IOException ex) {
                        System.out.println(ex);
                        Logger.getLogger(MarghController.class.getName()).log(Level.SEVERE, null, ex);
                        ex.printStackTrace();
                    }
                });

                deleteButton.setGraphic(delete);
                deleteButton.setTooltip(new Tooltip("Delete"));
                deleteButton.getStyleClass().add("del-btn");
                deleteButton.setOnAction(event -> {
                    Margh getMargh = getTableView().getItems().get(getIndex());
                    deleteMargh(getMargh.getId());
                });

            }

            @Override
            protected void updateItem(Void item,
                    boolean empty) {
                super.updateItem(item, empty);

                setGraphic(empty ? null : pane);
            }
        });
    }

    public void loadViewMargh(int mId) {
        try {
            Connection con = Service.ConnectDB();

            String viewMargh = "SELECT MARGH.ID, MARGH.TOLE_ID, PROVINCE.PROVINCE, MUNICIPALITY.MUNICIPALITY, WARD.WARD, TOLE.TOLE, MARGH.MARGH, MARGH.DESCRIPTION, MARGH.STATUS, MARGH.CREATED_AT FROM MARGH INNER JOIN TOLE ON TOLE.ID = MARGH.TOLE_ID INNER JOIN WARD ON WARD.ID = TOLE.WARD_ID INNER JOIN MUNICIPALITY ON MUNICIPALITY.ID = WARD.MUN_ID INNER JOIN PROVINCE ON PROVINCE.ID = MUNICIPALITY.PROV_ID WHERE MARGH.ID = ?";

            PreparedStatement pst = con.prepareStatement(viewMargh);
            pst.setInt(1, mId);

            ResultSet rs = pst.executeQuery();
            while (rs.next()) {

                vProvince.setText(rs.getString("PROVINCE"));
                vMunicipality.setText(rs.getString("MUNICIPALITY"));
                vWard.setText(String.valueOf(rs.getInt("WARD")));
                vTole.setText(rs.getString("TOLE"));
                vMargh.setText(rs.getString("MARGH"));
                vDesc.setWrapText(true);
                vDesc.setTextAlignment(TextAlignment.JUSTIFY);
                vDesc.setText(rs.getString("DESCRIPTION"));

                vStatus.setText(String.valueOf(rs.getBoolean("STATUS")));

                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                Date cdate = rs.getTimestamp("CREATED_AT");
                vCreatedAt.setText(dateFormat.format(cdate));
            }
        } catch (SQLException se) {
            System.out.println(se);
            Logger.getLogger(MarghController.class.getName()).log(Level.SEVERE, null, se);
            se.printStackTrace();
        }
    }

    public void loadUpdateMargh(int mId) {
        loadProvData();
        try {
            Connection con = Service.ConnectDB();

            String viewMargh = "SELECT MARGH.ID, MARGH.TOLE_ID, PROVINCE.PROVINCE, MUNICIPALITY.MUNICIPALITY, WARD.WARD, TOLE.TOLE, MARGH.MARGH, MARGH.DESCRIPTION, MARGH.STATUS, MARGH.CREATED_AT FROM MARGH INNER JOIN TOLE ON TOLE.ID = MARGH.TOLE_ID INNER JOIN WARD ON WARD.ID = TOLE.WARD_ID INNER JOIN MUNICIPALITY ON MUNICIPALITY.ID = WARD.MUN_ID INNER JOIN PROVINCE ON PROVINCE.ID = MUNICIPALITY.PROV_ID WHERE MARGH.ID = ?";

            PreparedStatement pst = con.prepareStatement(viewMargh);
            pst.setInt(1, mId);

            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                updateTitle.setText(rs.getString("MARGH"));
                cMargh.setText(rs.getString("MARGH"));
                cDesc.setText(rs.getString("DESCRIPTION"));
            }

        } catch (SQLException se) {
            System.out.println(se);
            Logger.getLogger(MarghController.class.getName()).log(Level.SEVERE, null, se);
            se.printStackTrace();
        }
    }

    public void deleteMargh(int mId) {
        JFXDialog jfxDialog = new JFXDialog();
        JFXDialogLayout content = new JFXDialogLayout();
        VBox vBox = new VBox();
        vBox.setSpacing(10);
        HBox hBox = new HBox();
        Label label = new Label("Are you sure you want to delete!");
        hBox.getChildren().addAll(label);
        HBox hBox2 = new HBox();
        JFXButton jfxButton1 = new JFXButton("Cancel");
        jfxButton1.setStyle("-fx-background-color: #eee; -fx-text-fill: #333");
        JFXButton jfxButton2 = new JFXButton("Yes");
        jfxButton2.setStyle("-fx-background-color: red; -fx-text-fill: white");

        Insets buttonInset = new Insets(0, 0, 0, 10);
        hBox2.setMargin(jfxButton2, buttonInset);
        hBox2.getChildren().addAll(jfxButton1, jfxButton2);
        vBox.getChildren().addAll(hBox, hBox2);
        content.setBody(vBox);
        jfxDialog.setContent(content);
        jfxDialog.setDialogContainer(marghStackPane);
        jfxDialog.show();

        EventHandler<ActionEvent> cancelDel = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                jfxDialog.close();
            }
        };
        jfxButton1.setOnAction(cancelDel);

        EventHandler<ActionEvent> confirmDel = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
//                String delMargh = "UPDATE MARGH SET STATUS =? WHERE ID =?";
                String delMargh = "DELETE FROM MARGH WHERE ID =?";
                PreparedStatement pstDel;
                try {
                    try (Connection con = Service.ConnectDB()) {

                        pstDel = con.prepareStatement(delMargh);
//                        pstDel.setBoolean(1, false);
                        pstDel.setInt(1, mId);

                        int rowsDeleted = pstDel.executeUpdate();

                        if (rowsDeleted > 0) {
                            FontAwesomeIconView tick = new FontAwesomeIconView(FontAwesomeIcon.CHECK_CIRCLE);
                            tick.setStyle("-fx-fill: LIGHTGREEN");
                            tick.setGlyphSize(50);
                            Notifications notify = Notifications.create()
                                    .title("Success")
                                    .text("Margh successfully removed.")
                                    .graphic(tick)
                                    .hideAfter(Duration.seconds(5))
                                    .position(Pos.TOP_RIGHT);
                            notify.darkStyle();
                            notify.show();

                        } else {
                            FontAwesomeIconView tick = new FontAwesomeIconView(FontAwesomeIcon.TIMES_CIRCLE);
                            tick.setStyle("-fx-fill: ORANGERED");
                            tick.setGlyphSize(50);
                            Notifications notify = Notifications.create()
                                    .title("Invalid")
                                    .text("Error occured while deleting margh.")
                                    .graphic(tick)
                                    .hideAfter(Duration.seconds(5))
                                    .position(Pos.TOP_RIGHT);
                            notify.darkStyle();
                            notify.show();
                        }

                        con.commit();
                        con.close();
                    }

//                    tbMarghList.getItems().clear();
                    tbMarghList.setItems(null);
                    loadMarghData();

                } catch (HeadlessException | SQLException ex) {
                    FontAwesomeIconView tick = new FontAwesomeIconView(FontAwesomeIcon.TIMES_CIRCLE);
                    tick.setStyle("-fx-fill: ORANGERED");
                    tick.setGlyphSize(50);
                    Notifications notify = Notifications.create()
                            .title("Invalid")
                            .text("Error occured while deleting margh.")
                            .graphic(tick)
                            .hideAfter(Duration.seconds(5))
                            .position(Pos.TOP_RIGHT);
                    notify.darkStyle();
                    notify.show();
                    System.out.print(ex);
                    Logger.getLogger(MarghController.class.getName()).log(Level.SEVERE, null, ex);
                    ex.printStackTrace();
                }

                jfxDialog.close();
            }
        };
        jfxButton2.setOnAction(confirmDel);

    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url,
            ResourceBundle rb) {
        // TODO
    }

    @FXML
    private void gotoDash(MouseEvent event) {
        try {
            contentPane.getChildren().clear();
            Pane newLoadedPane = FXMLLoader.load(getClass().getResource("/microservice/view/Dashboard.fxml"));

            contentPane.getChildren().add(newLoadedPane);
        } catch (IOException e) {
            System.out.print(e);
        }
    }
}
