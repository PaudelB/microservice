/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package microservice.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.validation.RequiredFieldValidator;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import java.awt.HeadlessException;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;
import javafx.util.Duration;
import javafx.util.StringConverter;
import org.controlsfx.control.Notifications;
import microservice.model.Municipality;
import microservice.model.Province;
import microservice.service.Service;

/**
 * FXML Controller class
 *
 * @author Dell
 */
public class MunicipalityController implements Initializable {

    private final MunicipalityController mc = this;

    private int cProvId;

    private int munId;

    @FXML
    private AnchorPane contentPane;

    @FXML
    private StackPane munStackPane;

    @FXML
    private Pane firstPane;

    @FXML
    private Pane secondPane;

    @FXML
    private JFXTextField cMunicipality;

    @FXML
    private JFXTextArea cDesc;

    @FXML
    private JFXComboBox<Province> cProv;

    @FXML
    private TableView<Municipality> tbMunList;

    @FXML
    private TableColumn<Municipality, Number> tbId;

    @FXML
    private TableColumn<Municipality, String> tbProvince;

    @FXML
    private TableColumn<Municipality, String> tbMunicipality;

    @FXML
    private TableColumn<Municipality, Void> tbAction;

    @FXML
    private Label vCreatedAt;

    @FXML
    private Label vDesc;

    @FXML
    private Label vProvince;

    @FXML
    private Label vStatus;

    @FXML
    private Label vMunicipality;

    @FXML
    private Label updateTitle;

    @FXML
    void doneView(MouseEvent event) {
        try {
            firstPane.getChildren().clear();
            FXMLLoader fFxmlLoader = new FXMLLoader(getClass().getResource("/microservice/view/municipality/Create.fxml"));
            fFxmlLoader.setController(mc);
            Pane fNewLoadedPane = fFxmlLoader.load();
            firstPane.getChildren().add(fNewLoadedPane);

            loadProvData();
        } catch (IOException ie) {
            System.out.print(ie);
            Logger.getLogger(MunicipalityController.class.getName()).log(Level.SEVERE, null, ie);
            ie.printStackTrace();
        }
    }

    @FXML
    void clear(MouseEvent event) {
        clearCreate();
    }

    public void clearCreate() {
        cProv.setValue(null);
        cMunicipality.clear();
        cDesc.clear();
    }

    @FXML
    void create(MouseEvent event) {
        String addMun = "INSERT INTO MUNICIPALITY (PROV_ID, MUNICIPALITY, DESCRIPTION, STATUS, CREATED_AT) VALUES (?, ?, ?, ?, ?)";

        RequiredFieldValidator empValid = new RequiredFieldValidator();
        empValid.setMessage("Field value required.");

        cProv.setValidators(empValid);
        cMunicipality.setValidators(empValid);

        if (cProv.validate() && cMunicipality.validate()) {
            try {
                Timestamp createdAt = new Timestamp(System.currentTimeMillis());

                try (Connection con = Service.ConnectDB()) {
                    PreparedStatement statement = con.prepareStatement(addMun, Statement.RETURN_GENERATED_KEYS);
                    statement.setInt(1, cProvId);
                    statement.setString(2, cMunicipality.getText());
                    statement.setString(3, cDesc.getText());
                    statement.setBoolean(4, true);
                    statement.setTimestamp(5, createdAt);

                    statement.executeUpdate();
                    ResultSet rs = statement.getGeneratedKeys();

                    if (rs.next()) {
                        int generatedKey = rs.getInt(1);
                        FontAwesomeIconView tick = new FontAwesomeIconView(FontAwesomeIcon.CHECK_CIRCLE);
                        tick.setStyle("-fx-fill: LIGHTGREEN");
                        tick.setGlyphSize(50);
                        Notifications notify = Notifications.create()
                                .title("Success")
                                .text("New municipality successfully added.")
                                .graphic(tick)
                                .hideAfter(Duration.seconds(5))
                                .position(Pos.TOP_RIGHT);
                        notify.darkStyle();
                        notify.show();

                        tbMunList.getItems().clear();
                        loadMunData();

                        clearCreate();

                        firstPane.getChildren().clear();
                        FXMLLoader fFxmlLoader = new FXMLLoader(getClass().getResource("/microservice/view/municipality/View.fxml"));
                        fFxmlLoader.setController(mc);
                        Pane fNewLoadedPane = fFxmlLoader.load();
                        firstPane.getChildren().add(fNewLoadedPane);

                        loadViewMun(generatedKey);

                    } else {
                        FontAwesomeIconView tick = new FontAwesomeIconView(FontAwesomeIcon.TIMES_CIRCLE);
                        tick.setStyle("-fx-fill: ORANGERED");
                        tick.setGlyphSize(50);
                        Notifications notify = Notifications.create()
                                .title("Failed")
                                .text("Error occured while adding municipality!")
                                .graphic(tick)
                                .hideAfter(Duration.seconds(5))
                                .position(Pos.TOP_RIGHT);
                        notify.darkStyle();
                        notify.show();
                    }

                    con.commit();
                    con.close();
                }
            } catch (HeadlessException | SQLException | IOException ex) {
                FontAwesomeIconView tick = new FontAwesomeIconView(FontAwesomeIcon.TIMES_CIRCLE);
                tick.setStyle("-fx-fill: ORANGERED");
                tick.setGlyphSize(50);
                Notifications notify = Notifications.create()
                        .title("Failed")
                        .text("Error occured while adding municipality!")
                        .graphic(tick)
                        .hideAfter(Duration.seconds(5))
                        .position(Pos.TOP_RIGHT);
                notify.darkStyle();
                notify.show();

                System.out.print(ex);
                Logger.getLogger(MunicipalityController.class.getName()).log(Level.SEVERE, null, ex);
                ex.printStackTrace();
            }
        }
    }

    public void paneSetting() {
        try {
            firstPane.getChildren().clear();
            FXMLLoader fFxmlLoader = new FXMLLoader(getClass().getResource("/microservice/view/municipality/Create.fxml"));
            fFxmlLoader.setController(mc);
            Pane fNewLoadedPane = fFxmlLoader.load();
            firstPane.getChildren().add(fNewLoadedPane);

            loadProvData();

            secondPane.getChildren().clear();
            FXMLLoader sFxmlLoader = new FXMLLoader(getClass().getResource("/microservice/view/municipality/Data.fxml"));
            sFxmlLoader.setController(mc);
            Pane sNewLoadedPane = sFxmlLoader.load();
            secondPane.getChildren().add(sNewLoadedPane);

            loadMunData();

        } catch (IOException ie) {
            System.out.print(ie);
            Logger.getLogger(MunicipalityController.class.getName()).log(Level.SEVERE, null, ie);
            ie.printStackTrace();
        }
    }

    public void loadProvData() {
        cProv.setConverter(new StringConverter<Province>() {
            @Override
            public String toString(Province object) {
                return object.getProvince();
            }

            @Override
            public Province fromString(String string) {
                return null;
            }
        });

        ObservableList<Province> provData = FXCollections.observableArrayList();
        try {
            Connection con = Service.ConnectDB();

            String allUser = "SELECT * FROM PROVINCE WHERE STATUS = ?";

            PreparedStatement pst = con.prepareStatement(allUser);
            pst.setBoolean(1, true);

            ResultSet rs = pst.executeQuery();

            while (rs.next()) {
                provData.add(new Province(rs.getInt("ID"), rs.getString("PROVINCE"), rs.getString("DESCRIPTION"), rs.getBoolean("STATUS"), rs.getDate("CREATED_AT")));
            }
            cProv.getItems().addAll(provData);

        } catch (SQLException se) {
            System.out.println(se);
            Logger.getLogger(MunicipalityController.class.getName()).log(Level.SEVERE, null, se);
            se.printStackTrace();

        }

        cProv.valueProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                cProvId = newValue.getId();
//                System.out.println(newValue.getId() + "-" + newValue.getProvince());
            }
        });
    }

    public void loadMunData() {
        ObservableList<Municipality> munData = FXCollections.observableArrayList();
        try {
            Connection con = Service.ConnectDB();

            String allMun = "SELECT MUNICIPALITY.ID, MUNICIPALITY.PROV_ID, PROVINCE.PROVINCE, MUNICIPALITY.MUNICIPALITY, MUNICIPALITY.DESCRIPTION, MUNICIPALITY.STATUS, MUNICIPALITY.CREATED_AT FROM MUNICIPALITY INNER JOIN PROVINCE ON PROVINCE.ID = MUNICIPALITY.PROV_ID WHERE MUNICIPALITY.STATUS = ? ORDER BY PROVINCE.PROVINCE DESC";

            PreparedStatement pst = con.prepareStatement(allMun);
            pst.setBoolean(1, true);

            ResultSet rs = pst.executeQuery();

            while (rs.next()) {
                munData.add(new Municipality(rs.getInt("ID"), rs.getInt("PROV_ID"), rs.getString("PROVINCE"), rs.getString("MUNICIPALITY"), rs.getString("DESCRIPTION"), rs.getBoolean("STATUS"), rs.getDate("CREATED_AT")));
            }

            tbMunList.getItems().addAll(munData);

        } catch (SQLException se) {
            System.out.println(se);
            Logger.getLogger(MunicipalityController.class.getName()).log(Level.SEVERE, null, se);
            se.printStackTrace();

        }

        tbId.setSortable(false);
        tbId.setCellValueFactory(column -> new ReadOnlyObjectWrapper<Number>(tbMunList.getItems().indexOf(column.getValue()) + 1));
        tbProvince.setCellValueFactory(new PropertyValueFactory<>("province"));
        tbMunicipality.setCellValueFactory(new PropertyValueFactory<>("municipality"));

        tbAction.setCellFactory(param -> new TableCell<Municipality, Void>() {
            private final Button viewButton = new Button();
            private final Button editButton = new Button();
            private final Button deleteButton = new Button();
            private final HBox pane = new HBox(viewButton, editButton, deleteButton);

            {
                HBox.setMargin(editButton, new Insets(0, 0, 0, 2));
                HBox.setMargin(deleteButton, new Insets(0, 0, 0, 2));
                FontAwesomeIconView eye = new FontAwesomeIconView(FontAwesomeIcon.EYE);
                eye.setStyle("-fx-fill: DODGERBLUE");
                eye.setGlyphSize(12);

                FontAwesomeIconView edit = new FontAwesomeIconView(FontAwesomeIcon.EDIT);
                edit.setStyle("-fx-fill: DARKGREEN");
                edit.setGlyphSize(12);

                FontAwesomeIconView delete = new FontAwesomeIconView(FontAwesomeIcon.TRASH);
                delete.setStyle("-fx-fill: ORANGERED");
                delete.setGlyphSize(12);

                viewButton.setGraphic(eye);
                viewButton.setTooltip(new Tooltip("View"));
                viewButton.getStyleClass().add("eye-btn");
                viewButton.setOnAction(event -> {
                    try {
                        Municipality getMun = getTableView().getItems().get(getIndex());

                        firstPane.getChildren().clear();
                        FXMLLoader fFxmlLoader = new FXMLLoader(getClass().getResource("/microservice/view/municipality/View.fxml"));
                        fFxmlLoader.setController(mc);
                        Pane fNewLoadedPane = fFxmlLoader.load();
                        firstPane.getChildren().add(fNewLoadedPane);

                        loadViewMun(getMun.getId());
                    } catch (IOException ex) {
                        System.out.println(ex);
                        Logger.getLogger(MunicipalityController.class.getName()).log(Level.SEVERE, null, ex);
                        ex.printStackTrace();
                    }
                });

                editButton.setGraphic(edit);
                editButton.setTooltip(new Tooltip("Edit"));
                editButton.getStyleClass().add("edit-btn");
                editButton.setOnAction(event -> {

                    try {
                        Municipality getMun = getTableView().getItems().get(getIndex());

                        firstPane.getChildren().clear();
                        FXMLLoader fFxmlLoader = new FXMLLoader(getClass().getResource("/microservice/view/municipality/Update.fxml"));
                        fFxmlLoader.setController(mc);
                        Pane fNewLoadedPane = fFxmlLoader.load();
                        firstPane.getChildren().add(fNewLoadedPane);

                        loadUpdateMun(getMun.getId());
                        munId = getMun.getId();
                    } catch (IOException ex) {
                        System.out.println(ex);
                        Logger.getLogger(MunicipalityController.class.getName()).log(Level.SEVERE, null, ex);
                        ex.printStackTrace();
                    }
                });

                deleteButton.setGraphic(delete);
                deleteButton.setTooltip(new Tooltip("Delete"));
                deleteButton.getStyleClass().add("del-btn");
                deleteButton.setOnAction(event -> {
                    Municipality getMun = getTableView().getItems().get(getIndex());
                    deleteMun(getMun.getId());
                });
            }

            @Override
            protected void updateItem(Void item,
                    boolean empty) {
                super.updateItem(item, empty);

                setGraphic(empty ? null : pane);
            }
        });
    }

    public void loadViewMun(int mId) {
        try {
            Connection con = Service.ConnectDB();

            String viewMun = "SELECT PROVINCE.PROVINCE, MUNICIPALITY.MUNICIPALITY, MUNICIPALITY.DESCRIPTION, MUNICIPALITY.STATUS, MUNICIPALITY.CREATED_AT FROM MUNICIPALITY INNER JOIN PROVINCE ON PROVINCE.ID = MUNICIPALITY.PROV_ID WHERE MUNICIPALITY.ID = ?";

            PreparedStatement pst = con.prepareStatement(viewMun);
            pst.setInt(1, mId);

            ResultSet rs = pst.executeQuery();
            while (rs.next()) {

                vProvince.setText(rs.getString("PROVINCE"));
                vMunicipality.setText(rs.getString("MUNICIPALITY"));
                vDesc.setWrapText(true);
                vDesc.setTextAlignment(TextAlignment.JUSTIFY);
                vDesc.setText(rs.getString("DESCRIPTION"));

                vStatus.setText(String.valueOf(rs.getBoolean("STATUS")));

                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                Date cdate = rs.getTimestamp("CREATED_AT");
                vCreatedAt.setText(dateFormat.format(cdate));
            }
        } catch (SQLException se) {
            System.out.println(se);
            Logger.getLogger(MunicipalityController.class.getName()).log(Level.SEVERE, null, se);
            se.printStackTrace();
        }
    }

    public void loadUpdateMun(int mId) {
        loadProvData();
        Province provData = null;
        try {
            Connection con = Service.ConnectDB();

            String viewMun = "SELECT PROVINCE.ID AS PID, PROVINCE.PROVINCE, MUNICIPALITY.MUNICIPALITY, MUNICIPALITY.DESCRIPTION, PROVINCE.DESCRIPTION AS PDIS, PROVINCE.STATUS, PROVINCE.CREATED_AT FROM MUNICIPALITY INNER JOIN PROVINCE ON PROVINCE.ID = MUNICIPALITY.PROV_ID WHERE MUNICIPALITY.ID = ?";

            PreparedStatement pst = con.prepareStatement(viewMun);
            pst.setInt(1, mId);

            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                provData = new Province(rs.getInt("PID"), rs.getString("PROVINCE"), rs.getString("PDIS"), rs.getBoolean("STATUS"), rs.getDate("CREATED_AT"));
                updateTitle.setText(rs.getString("MUNICIPALITY"));
                cMunicipality.setText(rs.getString("MUNICIPALITY"));
                cDesc.setText(rs.getString("DESCRIPTION"));
            }
//            System.out.println(provData.getProvince());
//            cProv.getSelectionModel().select(provData);

        } catch (SQLException se) {
            System.out.println(se);
            Logger.getLogger(MunicipalityController.class.getName()).log(Level.SEVERE, null, se);
            se.printStackTrace();
        }
    }

    @FXML
    void cancel(MouseEvent event) {
        try {
            firstPane.getChildren().clear();
            FXMLLoader fFxmlLoader = new FXMLLoader(getClass().getResource("/microservice/view/municipality/Create.fxml"));
            fFxmlLoader.setController(mc);
            Pane fNewLoadedPane = fFxmlLoader.load();
            firstPane.getChildren().add(fNewLoadedPane);

            loadProvData();
        } catch (IOException ie) {
            System.out.print(ie);
            Logger.getLogger(MunicipalityController.class.getName()).log(Level.SEVERE, null, ie);
            ie.printStackTrace();
        }
    }

    @FXML
    void update(MouseEvent event) {
        String updateProv = "UPDATE MUNICIPALITY SET PROV_ID = ?, MUNICIPALITY = ?, DESCRIPTION = ?, CREATED_AT = ? WHERE ID = ?";

        RequiredFieldValidator empValid = new RequiredFieldValidator();
        empValid.setMessage("Field value required.");

        cProv.setValidators(empValid);
        cMunicipality.setValidators(empValid);

        if (cProv.validate() && cMunicipality.validate()) {
            try {
                Timestamp createdAt = new Timestamp(System.currentTimeMillis());
                int rowsUpdated;
                try (Connection con = Service.ConnectDB()) {

                    PreparedStatement statement = con.prepareStatement(updateProv);
                    statement.setInt(1, cProvId);
                    statement.setString(2, cMunicipality.getText());
                    statement.setString(3, cDesc.getText());
                    statement.setTimestamp(4, createdAt);

                    statement.setLong(5, munId);

                    rowsUpdated = statement.executeUpdate();

                    if (rowsUpdated > 0) {
                        FontAwesomeIconView tick = new FontAwesomeIconView(FontAwesomeIcon.CHECK_CIRCLE);
                        tick.setStyle("-fx-fill: LIGHTGREEN");
                        tick.setGlyphSize(50);
                        Notifications notify = Notifications.create()
                                .title("Success")
                                .text("Municipality updated successfully.")
                                .graphic(tick)
                                .hideAfter(Duration.seconds(5))
                                .position(Pos.TOP_RIGHT);
                        notify.darkStyle();
                        notify.show();

                        tbMunList.getItems().clear();
                        loadMunData();
                        
                        clearCreate();

                        firstPane.getChildren().clear();
                        FXMLLoader fFxmlLoader = new FXMLLoader(getClass().getResource("/microservice/view/municipality/View.fxml"));
                        fFxmlLoader.setController(mc);
                        Pane fNewLoadedPane = fFxmlLoader.load();
                        firstPane.getChildren().add(fNewLoadedPane);

                        loadViewMun(munId);

                    } else {
                        FontAwesomeIconView tick = new FontAwesomeIconView(FontAwesomeIcon.TIMES_CIRCLE);
                        tick.setStyle("-fx-fill: ORANGERED");
                        tick.setGlyphSize(50);
                        Notifications notify = Notifications.create()
                                .title("Failed")
                                .text("Error occured while updating municipality!")
                                .graphic(tick)
                                .hideAfter(Duration.seconds(5))
                                .position(Pos.TOP_RIGHT);
                        notify.darkStyle();
                        notify.show();
                    }

                    con.commit();
                    con.close();
                }

            } catch (HeadlessException | SQLException | IOException ex) {
                FontAwesomeIconView tick = new FontAwesomeIconView(FontAwesomeIcon.TIMES_CIRCLE);
                tick.setStyle("-fx-fill: ORANGERED");
                tick.setGlyphSize(50);
                Notifications notify = Notifications.create()
                        .title("Failed")
                        .text("Error occured while updating municipality!")
                        .graphic(tick)
                        .hideAfter(Duration.seconds(5))
                        .position(Pos.TOP_RIGHT);
                notify.darkStyle();
                notify.show();

                System.out.print(ex);
                Logger.getLogger(MunicipalityController.class.getName()).log(Level.SEVERE, null, ex);
                ex.printStackTrace();
            }
        }
    }

    public void deleteMun(int mId) {
        JFXDialog jfxDialog = new JFXDialog();
        JFXDialogLayout content = new JFXDialogLayout();
        VBox vBox = new VBox();
        vBox.setSpacing(10);
        HBox hBox = new HBox();
        Label label = new Label("Are you sure you want to delete!");
        hBox.getChildren().addAll(label);
        HBox hBox2 = new HBox();
        JFXButton jfxButton1 = new JFXButton("Cancel");
        jfxButton1.setStyle("-fx-background-color: #eee; -fx-text-fill: #333");
        JFXButton jfxButton2 = new JFXButton("Yes");
        jfxButton2.setStyle("-fx-background-color: red; -fx-text-fill: white");

        Insets buttonInset = new Insets(0, 0, 0, 10);
        hBox2.setMargin(jfxButton2, buttonInset);
        hBox2.getChildren().addAll(jfxButton1, jfxButton2);
        vBox.getChildren().addAll(hBox, hBox2);
        content.setBody(vBox);
        jfxDialog.setContent(content);
        jfxDialog.setDialogContainer(munStackPane);
        jfxDialog.show();

        EventHandler<ActionEvent> cancelDel = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                jfxDialog.close();
            }
        };
        jfxButton1.setOnAction(cancelDel);

        EventHandler<ActionEvent> confirmDel = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
//                String delMun = "UPDATE MUNICIPALITY SET STATUS =? WHERE ID =?";
                String delMun = "DELETE FROM MUNICIPALITY WHERE ID =?";
                PreparedStatement pstDel;
                try {
                    try (Connection con = Service.ConnectDB()) {

                        pstDel = con.prepareStatement(delMun);
//                        pstDel.setBoolean(1, false);
                        pstDel.setInt(1, mId);

                        int rowsDeleted = pstDel.executeUpdate();

                        if (rowsDeleted > 0) {
                            FontAwesomeIconView tick = new FontAwesomeIconView(FontAwesomeIcon.CHECK_CIRCLE);
                            tick.setStyle("-fx-fill: LIGHTGREEN");
                            tick.setGlyphSize(50);
                            Notifications notify = Notifications.create()
                                    .title("Success")
                                    .text("Municipality successfully removed.")
                                    .graphic(tick)
                                    .hideAfter(Duration.seconds(5))
                                    .position(Pos.TOP_RIGHT);
                            notify.darkStyle();
                            notify.show();

                        } else {
                            FontAwesomeIconView tick = new FontAwesomeIconView(FontAwesomeIcon.TIMES_CIRCLE);
                            tick.setStyle("-fx-fill: ORANGERED");
                            tick.setGlyphSize(50);
                            Notifications notify = Notifications.create()
                                    .title("Invalid")
                                    .text("Error occured while deleting Municipality.")
                                    .graphic(tick)
                                    .hideAfter(Duration.seconds(5))
                                    .position(Pos.TOP_RIGHT);
                            notify.darkStyle();
                            notify.show();
                        }

                        con.commit();
                        con.close();
                    }

                    tbMunList.getItems().clear();
                    loadMunData();

                } catch (HeadlessException | SQLException ex) {
                    FontAwesomeIconView tick = new FontAwesomeIconView(FontAwesomeIcon.TIMES_CIRCLE);
                    tick.setStyle("-fx-fill: ORANGERED");
                    tick.setGlyphSize(50);
                    Notifications notify = Notifications.create()
                            .title("Invalid")
                            .text("Error occured while deleting Municipality.")
                            .graphic(tick)
                            .hideAfter(Duration.seconds(5))
                            .position(Pos.TOP_RIGHT);
                    notify.darkStyle();
                    notify.show();
                    System.out.print(ex);
                    Logger.getLogger(ProvinceController.class.getName()).log(Level.SEVERE, null, ex);
                    ex.printStackTrace();
                }

                jfxDialog.close();
            }
        };
        jfxButton2.setOnAction(confirmDel);

    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url,
            ResourceBundle rb) {
        // TODO
    }

    @FXML
    private void gotoDash(MouseEvent event) {
        try {
            contentPane.getChildren().clear();
            Pane newLoadedPane = FXMLLoader.load(getClass().getResource("/microservice/view/Dashboard.fxml"));

            contentPane.getChildren().add(newLoadedPane);
        } catch (IOException e) {
            System.out.print(e);
        }
    }

}
