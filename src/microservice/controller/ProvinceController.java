/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package microservice.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.validation.RequiredFieldValidator;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import java.awt.HeadlessException;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;
import javafx.util.Duration;
import org.controlsfx.control.Notifications;
import microservice.model.Province;
import microservice.service.Service;

/**
 * FXML Controller class
 *
 * @author Dell
 */
public class ProvinceController implements Initializable {

    private final ProvinceController pc = this;

    private int provId;

    @FXML
    private AnchorPane contentPane;

    @FXML
    private StackPane provStackPane;

    @FXML
    private Pane firstPane;

    @FXML
    private Pane secondPane;

    @FXML
    private JFXTextField cProvince;

    @FXML
    private JFXTextArea cDesc;

    @FXML
    private TableView<Province> tbProvList;

    @FXML
    private TableColumn<Province, Number> tbId;

    @FXML
    private TableColumn<Province, String> tbProvince;

    @FXML
    private TableColumn<Province, String> tbDesc;

    @FXML
    private TableColumn<Province, Void> tbAction;

    @FXML
    private JFXTextField uProvince;

    @FXML
    private JFXTextArea uDesc;

    @FXML
    private Label updateTitle;

    @FXML
    private Label vCreatedAt;

    @FXML
    private Label vDesc;

    @FXML
    private Label vProvince;

    @FXML
    private Label vStatus;

    public void paneSetting() {
        try {
            firstPane.getChildren().clear();
            FXMLLoader fFxmlLoader = new FXMLLoader(getClass().getResource("/microservice/view/province/Create.fxml"));
            fFxmlLoader.setController(pc);
            Pane fNewLoadedPane = fFxmlLoader.load();
            firstPane.getChildren().add(fNewLoadedPane);

            secondPane.getChildren().clear();
            FXMLLoader sFxmlLoader = new FXMLLoader(getClass().getResource("/microservice/view/province/Province.fxml"));
            sFxmlLoader.setController(pc);
            Pane sNewLoadedPane = sFxmlLoader.load();
            secondPane.getChildren().add(sNewLoadedPane);

            loadProvData();
        } catch (IOException ie) {
            System.out.print(ie);
            Logger.getLogger(ProvinceController.class.getName()).log(Level.SEVERE, null, ie);
            ie.printStackTrace();
        }
    }

    @FXML
    void clear(MouseEvent event) {
        clearCreate();
    }

    @FXML
    void create(MouseEvent event) {

        String addProv = "INSERT INTO PROVINCE (PROVINCE, DESCRIPTION, STATUS, CREATED_AT) VALUES (?, ?, ?, ?)";

        RequiredFieldValidator empValid = new RequiredFieldValidator();
        empValid.setMessage("Field value required.");

        cProvince.setValidators(empValid);

        if (cProvince.validate()) {
            try {
                Timestamp createdAt = new Timestamp(System.currentTimeMillis());

                try (Connection con = Service.ConnectDB()) {
                    PreparedStatement statement = con.prepareStatement(addProv, Statement.RETURN_GENERATED_KEYS);
                    statement.setString(1, cProvince.getText());
                    statement.setString(2, cDesc.getText());
                    statement.setBoolean(3, true);
                    statement.setTimestamp(4, createdAt);

                    statement.executeUpdate();
                    ResultSet rs = statement.getGeneratedKeys();

                    if (rs.next()) {
                        int generatedKey = rs.getInt(1);
                        FontAwesomeIconView tick = new FontAwesomeIconView(FontAwesomeIcon.CHECK_CIRCLE);
                        tick.setStyle("-fx-fill: LIGHTGREEN");
                        tick.setGlyphSize(50);
                        Notifications notify = Notifications.create()
                                .title("Success")
                                .text("New province successfully added.")
                                .graphic(tick)
                                .hideAfter(Duration.seconds(5))
                                .position(Pos.TOP_RIGHT);
                        notify.darkStyle();
                        notify.show();

                        tbProvList.getItems().clear();
                        loadProvData();

                        clearCreate();

                        firstPane.getChildren().clear();
                        FXMLLoader fFxmlLoader = new FXMLLoader(getClass().getResource("/microservice/view/province/View.fxml"));
                        fFxmlLoader.setController(pc);
                        Pane fNewLoadedPane = fFxmlLoader.load();
                        firstPane.getChildren().add(fNewLoadedPane);

                        loadViewProv(generatedKey);

                    } else {
                        FontAwesomeIconView tick = new FontAwesomeIconView(FontAwesomeIcon.TIMES_CIRCLE);
                        tick.setStyle("-fx-fill: ORANGERED");
                        tick.setGlyphSize(50);
                        Notifications notify = Notifications.create()
                                .title("Failed")
                                .text("Error occured while adding province!")
                                .graphic(tick)
                                .hideAfter(Duration.seconds(5))
                                .position(Pos.TOP_RIGHT);
                        notify.darkStyle();
                        notify.show();
                    }

                    con.commit();
                    con.close();
                }
            } catch (HeadlessException | SQLException | IOException ex) {
                FontAwesomeIconView tick = new FontAwesomeIconView(FontAwesomeIcon.TIMES_CIRCLE);
                tick.setStyle("-fx-fill: ORANGERED");
                tick.setGlyphSize(50);
                Notifications notify = Notifications.create()
                        .title("Failed")
                        .text("Error occured while adding province!")
                        .graphic(tick)
                        .hideAfter(Duration.seconds(5))
                        .position(Pos.TOP_RIGHT);
                notify.darkStyle();
                notify.show();

                System.out.print(ex);
                Logger.getLogger(ProvinceController.class.getName()).log(Level.SEVERE, null, ex);
                ex.printStackTrace();
            }
        }
    }

    public void clearCreate() {
        cProvince.clear();
        cDesc.clear();
    }

    public void loadProvData() {
        ObservableList<Province> provData = FXCollections.observableArrayList();
        try {
            Connection con = Service.ConnectDB();

            String allUser = "SELECT * FROM PROVINCE WHERE STATUS = ?";

            PreparedStatement pst = con.prepareStatement(allUser);
            pst.setBoolean(1, true);

            ResultSet rs = pst.executeQuery();

            while (rs.next()) {
                provData.add(new Province(rs.getInt("ID"), rs.getString("PROVINCE"), rs.getString("DESCRIPTION"), rs.getBoolean("STATUS"), rs.getDate("CREATED_AT")));
            }

            tbProvList.getItems().addAll(provData);

        } catch (SQLException se) {
            System.out.println(se);
            Logger.getLogger(ProvinceController.class.getName()).log(Level.SEVERE, null, se);
            se.printStackTrace();

        }

        tbId.setSortable(false);
        tbId.setCellValueFactory(column -> new ReadOnlyObjectWrapper<Number>(tbProvList.getItems().indexOf(column.getValue()) + 1));
        tbProvince.setCellValueFactory(new PropertyValueFactory<>("province"));
        tbDesc.setCellValueFactory(new PropertyValueFactory<>("description"));

        tbAction.setCellFactory(param -> new TableCell<Province, Void>() {
            private final Button viewButton = new Button();
            private final Button editButton = new Button();
            private final Button deleteButton = new Button();
            private final HBox pane = new HBox(viewButton, editButton, deleteButton);

            {
                HBox.setMargin(editButton, new Insets(0, 0, 0, 2));
                HBox.setMargin(deleteButton, new Insets(0, 0, 0, 2));
                FontAwesomeIconView eye = new FontAwesomeIconView(FontAwesomeIcon.EYE);
                eye.setStyle("-fx-fill: DODGERBLUE");
                eye.setGlyphSize(12);

                FontAwesomeIconView edit = new FontAwesomeIconView(FontAwesomeIcon.EDIT);
                edit.setStyle("-fx-fill: DARKGREEN");
                edit.setGlyphSize(12);

                FontAwesomeIconView delete = new FontAwesomeIconView(FontAwesomeIcon.TRASH);
                delete.setStyle("-fx-fill: ORANGERED");
                delete.setGlyphSize(12);

                viewButton.setGraphic(eye);
                viewButton.setTooltip(new Tooltip("View"));
                viewButton.getStyleClass().add("eye-btn");
                viewButton.setOnAction(event -> {
                    try {
                        Province getProv = getTableView().getItems().get(getIndex());

                        firstPane.getChildren().clear();
                        FXMLLoader fFxmlLoader = new FXMLLoader(getClass().getResource("/microservice/view/province/View.fxml"));
                        fFxmlLoader.setController(pc);
                        Pane fNewLoadedPane = fFxmlLoader.load();
                        firstPane.getChildren().add(fNewLoadedPane);

                        loadViewProv(getProv.getId());
                    } catch (IOException ex) {
                        System.out.println(ex);
                        Logger.getLogger(ProvinceController.class.getName()).log(Level.SEVERE, null, ex);
                        ex.printStackTrace();
                    }
                });

                editButton.setGraphic(edit);
                editButton.setTooltip(new Tooltip("Edit"));
                editButton.getStyleClass().add("edit-btn");
                editButton.setOnAction(event -> {

                    try {
                        Province getProv = getTableView().getItems().get(getIndex());

                        firstPane.getChildren().clear();
                        FXMLLoader fFxmlLoader = new FXMLLoader(getClass().getResource("/microservice/view/province/Update.fxml"));
                        fFxmlLoader.setController(pc);
                        Pane fNewLoadedPane = fFxmlLoader.load();
                        firstPane.getChildren().add(fNewLoadedPane);

                        loadUpdateProv(getProv.getId());
                        provId = getProv.getId();
                    } catch (IOException ex) {
                        System.out.println(ex);
                        Logger.getLogger(ProvinceController.class.getName()).log(Level.SEVERE, null, ex);
                        ex.printStackTrace();
                    }
                });

                deleteButton.setGraphic(delete);
                deleteButton.setTooltip(new Tooltip("Delete"));
                deleteButton.getStyleClass().add("del-btn");
                deleteButton.setOnAction(event -> {
                    Province getProv = getTableView().getItems().get(getIndex());
                    deleteThisProv(getProv.getId());
                });
            }

            @Override
            protected void updateItem(Void item,
                    boolean empty) {
                super.updateItem(item, empty);

                setGraphic(empty ? null : pane);
            }
        });
    }

    public void loadUpdateProv(int pId) {

        try {
            Connection con = Service.ConnectDB();

            String viewProv = "SELECT * FROM PROVINCE WHERE ID =?";

            PreparedStatement pst = con.prepareStatement(viewProv);
            pst.setInt(1, pId);

            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                updateTitle.setText(rs.getString("PROVINCE"));
                uProvince.setText(rs.getString("PROVINCE"));
                uDesc.setText(rs.getString("DESCRIPTION"));
            }
        } catch (SQLException se) {
            System.out.println(se);
            Logger.getLogger(ProvinceController.class.getName()).log(Level.SEVERE, null, se);
            se.printStackTrace();
        }
    }

    @FXML
    void cancel(MouseEvent event) {
        try {
            firstPane.getChildren().clear();
            FXMLLoader fFxmlLoader = new FXMLLoader(getClass().getResource("/microservice/view/province/Create.fxml"));
            fFxmlLoader.setController(pc);
            Pane fNewLoadedPane = fFxmlLoader.load();
            firstPane.getChildren().add(fNewLoadedPane);

        } catch (IOException ie) {
            System.out.print(ie);
            Logger.getLogger(ProvinceController.class.getName()).log(Level.SEVERE, null, ie);
            ie.printStackTrace();
        }
    }

    @FXML
    void update(MouseEvent event) {
        String updateProv = "UPDATE PROVINCE SET PROVINCE = ?, DESCRIPTION = ?, CREATED_AT = ? WHERE ID = ?";

        RequiredFieldValidator empValid = new RequiredFieldValidator();
        empValid.setMessage("Field value required.");

        uProvince.setValidators(empValid);

        if (uProvince.validate()) {
            try {
                Timestamp createdAt = new Timestamp(System.currentTimeMillis());
                int rowsUpdated;
                try (Connection con = Service.ConnectDB()) {

                    PreparedStatement statement = con.prepareStatement(updateProv);
                    statement.setString(1, uProvince.getText());
                    statement.setString(2, uDesc.getText());
                    statement.setTimestamp(3, createdAt);

                    statement.setLong(4, provId);

                    rowsUpdated = statement.executeUpdate();

                    if (rowsUpdated > 0) {
                        FontAwesomeIconView tick = new FontAwesomeIconView(FontAwesomeIcon.CHECK_CIRCLE);
                        tick.setStyle("-fx-fill: LIGHTGREEN");
                        tick.setGlyphSize(50);
                        Notifications notify = Notifications.create()
                                .title("Success")
                                .text("Province updated successfully.")
                                .graphic(tick)
                                .hideAfter(Duration.seconds(5))
                                .position(Pos.TOP_RIGHT);
                        notify.darkStyle();
                        notify.show();

                        tbProvList.getItems().clear();
                        loadProvData();

                        firstPane.getChildren().clear();
                        FXMLLoader fFxmlLoader = new FXMLLoader(getClass().getResource("/microservice/view/province/View.fxml"));
                        fFxmlLoader.setController(pc);
                        Pane fNewLoadedPane = fFxmlLoader.load();
                        firstPane.getChildren().add(fNewLoadedPane);

                        loadViewProv(provId);

                    } else {
                        FontAwesomeIconView tick = new FontAwesomeIconView(FontAwesomeIcon.TIMES_CIRCLE);
                        tick.setStyle("-fx-fill: ORANGERED");
                        tick.setGlyphSize(50);
                        Notifications notify = Notifications.create()
                                .title("Failed")
                                .text("Error occured while updating province!")
                                .graphic(tick)
                                .hideAfter(Duration.seconds(5))
                                .position(Pos.TOP_RIGHT);
                        notify.darkStyle();
                        notify.show();
                    }

                    con.commit();
                    con.close();
                }

            } catch (HeadlessException | SQLException | IOException ex) {
                FontAwesomeIconView tick = new FontAwesomeIconView(FontAwesomeIcon.TIMES_CIRCLE);
                tick.setStyle("-fx-fill: ORANGERED");
                tick.setGlyphSize(50);
                Notifications notify = Notifications.create()
                        .title("Failed")
                        .text("Error occured while updating province!")
                        .graphic(tick)
                        .hideAfter(Duration.seconds(5))
                        .position(Pos.TOP_RIGHT);
                notify.darkStyle();
                notify.show();

                System.out.print(ex);
                Logger.getLogger(ProvinceController.class.getName()).log(Level.SEVERE, null, ex);
                ex.printStackTrace();
            }
        }
    }

    public void loadViewProv(int pId) {
        try {
            Connection con = Service.ConnectDB();

            String viewProv = "SELECT * FROM PROVINCE WHERE ID =?";

            PreparedStatement pst = con.prepareStatement(viewProv);
            pst.setInt(1, pId);

            ResultSet rs = pst.executeQuery();
            while (rs.next()) {

                vProvince.setText(rs.getString("PROVINCE"));

                vDesc.setWrapText(true);
                vDesc.setTextAlignment(TextAlignment.JUSTIFY);
                vDesc.setText(rs.getString("DESCRIPTION"));

                vStatus.setText(String.valueOf(rs.getBoolean("STATUS")));

                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                Date cdate = rs.getTimestamp("CREATED_AT");
                vCreatedAt.setText(dateFormat.format(cdate));
            }
        } catch (SQLException se) {
            System.out.println(se);
            Logger.getLogger(ProvinceController.class.getName()).log(Level.SEVERE, null, se);
            se.printStackTrace();
        }
    }

    @FXML
    void doneView(MouseEvent event) {
        try {
            firstPane.getChildren().clear();
            FXMLLoader fFxmlLoader = new FXMLLoader(getClass().getResource("/microservice/view/province/Create.fxml"));
            fFxmlLoader.setController(pc);
            Pane fNewLoadedPane = fFxmlLoader.load();
            firstPane.getChildren().add(fNewLoadedPane);

        } catch (IOException ie) {
            System.out.print(ie);
            Logger.getLogger(ProvinceController.class.getName()).log(Level.SEVERE, null, ie);
            ie.printStackTrace();
        }
    }

    public void deleteThisProv(int pId) {
        JFXDialog jfxDialog = new JFXDialog();
        JFXDialogLayout content = new JFXDialogLayout();
        VBox vBox = new VBox();
        vBox.setSpacing(10);
        HBox hBox = new HBox();
        Label label = new Label("Are you sure you want to delete!");
        hBox.getChildren().addAll(label);
        HBox hBox2 = new HBox();
        JFXButton jfxButton1 = new JFXButton("Cancel");
        jfxButton1.setStyle("-fx-background-color: #eee; -fx-text-fill: #333");
        JFXButton jfxButton2 = new JFXButton("Yes");
        jfxButton2.setStyle("-fx-background-color: red; -fx-text-fill: white");

        Insets buttonInset = new Insets(0, 0, 0, 10);
        hBox2.setMargin(jfxButton2, buttonInset);
        hBox2.getChildren().addAll(jfxButton1, jfxButton2);
        vBox.getChildren().addAll(hBox, hBox2);
        content.setBody(vBox);
        jfxDialog.setContent(content);
        jfxDialog.setDialogContainer(provStackPane);
        jfxDialog.show();

        EventHandler<ActionEvent> cancelDel = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                jfxDialog.close();
            }
        };
        jfxButton1.setOnAction(cancelDel);

        EventHandler<ActionEvent> confirmDel = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
//                String delProv = "UPDATE PROVINCE SET STATUS =? WHERE ID =?";
                String delProv = "DELETE FROM PROVINCE WHERE ID =?";
                PreparedStatement pstDel;
                try {
                    try (Connection con = Service.ConnectDB()) {

                        pstDel = con.prepareStatement(delProv);
//                        pstDel.setBoolean(1, false);
                        pstDel.setInt(1, pId);

                        int rowsDeleted = pstDel.executeUpdate();

                        if (rowsDeleted > 0) {
                            FontAwesomeIconView tick = new FontAwesomeIconView(FontAwesomeIcon.CHECK_CIRCLE);
                            tick.setStyle("-fx-fill: LIGHTGREEN");
                            tick.setGlyphSize(50);
                            Notifications notify = Notifications.create()
                                    .title("Success")
                                    .text("Province successfully removed.")
                                    .graphic(tick)
                                    .hideAfter(Duration.seconds(5))
                                    .position(Pos.TOP_RIGHT);
                            notify.darkStyle();
                            notify.show();

                        } else {
                            FontAwesomeIconView tick = new FontAwesomeIconView(FontAwesomeIcon.TIMES_CIRCLE);
                            tick.setStyle("-fx-fill: ORANGERED");
                            tick.setGlyphSize(50);
                            Notifications notify = Notifications.create()
                                    .title("Invalid")
                                    .text("Error occured while deleting province.")
                                    .graphic(tick)
                                    .hideAfter(Duration.seconds(5))
                                    .position(Pos.TOP_RIGHT);
                            notify.darkStyle();
                            notify.show();
                        }

                        con.commit();
                        con.close();
                    }

                    tbProvList.getItems().clear();
                    loadProvData();

                } catch (HeadlessException | SQLException ex) {
                    FontAwesomeIconView tick = new FontAwesomeIconView(FontAwesomeIcon.TIMES_CIRCLE);
                    tick.setStyle("-fx-fill: ORANGERED");
                    tick.setGlyphSize(50);
                    Notifications notify = Notifications.create()
                            .title("Invalid")
                            .text("Error occured while deleting province.")
                            .graphic(tick)
                            .hideAfter(Duration.seconds(5))
                            .position(Pos.TOP_RIGHT);
                    notify.darkStyle();
                    notify.show();
                    System.out.print(ex);
                    Logger.getLogger(ProvinceController.class.getName()).log(Level.SEVERE, null, ex);
                    ex.printStackTrace();
                }

                jfxDialog.close();
            }
        };
        jfxButton2.setOnAction(confirmDel);

    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url,
            ResourceBundle rb) {
        // TODO
    }

    @FXML
    private void gotoDash(MouseEvent event) {
        try {
            contentPane.getChildren().clear();
            Pane newLoadedPane = FXMLLoader.load(getClass().getResource("/microservice/view/Dashboard.fxml"));

            contentPane.getChildren().add(newLoadedPane);
        } catch (IOException e) {
            System.out.print(e);
        }
    }

}
