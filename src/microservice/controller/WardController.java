/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package microservice.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.validation.NumberValidator;
import com.jfoenix.validation.RequiredFieldValidator;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import java.awt.HeadlessException;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;
import javafx.util.Duration;
import javafx.util.StringConverter;
import org.controlsfx.control.Notifications;
import microservice.model.Municipality;
import microservice.model.Province;
import microservice.service.Service;
import microservice.model.Ward;

/**
 * FXML Controller class
 *
 * @author Dell
 */
public class WardController implements Initializable {

    private final WardController wc = this;

    private int cProvId;

    private int cMunId;

    private int wardId;

    @FXML
    private AnchorPane contentPane;

    @FXML
    private StackPane wardStackPane;

    @FXML
    private Pane firstPane;

    @FXML
    private Pane secondPane;

    @FXML
    private TableView<Ward> tbWardList;

    @FXML
    private TableColumn<Ward, Number> tbId;

    @FXML
    private TableColumn<Ward, String> tbProvince;

    @FXML
    private TableColumn<Ward, String> tbMunicipality;

    @FXML
    private TableColumn<Ward, Number> tbWard;

    @FXML
    private TableColumn<Ward, Void> tbAction;

    @FXML
    private JFXTextField cWard;

    @FXML
    private JFXTextArea cDesc;

    @FXML
    private JFXComboBox<Province> cProv;

    @FXML
    private JFXComboBox<Municipality> cMunicipality;

    @FXML
    private Label vCreatedAt;

    @FXML
    private Label vDesc;

    @FXML
    private Label vProvince;

    @FXML
    private Label vStatus;

    @FXML
    private Label vMunicipality;

    @FXML
    private Label vWard;

    @FXML
    private Label updateTitle;

    @FXML
    void doneView(MouseEvent event) {
        try {
            firstPane.getChildren().clear();
            FXMLLoader fFxmlLoader = new FXMLLoader(getClass().getResource("/microservice/view/ward/Create.fxml"));
            fFxmlLoader.setController(wc);
            Pane fNewLoadedPane = fFxmlLoader.load();
            firstPane.getChildren().add(fNewLoadedPane);

            loadProvData();
        } catch (IOException ie) {
            System.out.print(ie);
            Logger.getLogger(WardController.class.getName()).log(Level.SEVERE, null, ie);
            ie.printStackTrace();
        }
    }

    @FXML
    void clear(MouseEvent event) {
        clearCreate();
    }

    public void clearCreate() {
        cProv.setValue(null);
        cMunicipality.setValue(null);
        cWard.clear();
        cDesc.clear();
    }

    @FXML
    void create(MouseEvent event) {
        String addWard = "INSERT INTO WARD (MUN_ID, WARD, DESCRIPTION, STATUS, CREATED_AT) VALUES (?, ?, ?, ?, ?)";

        RequiredFieldValidator empValid = new RequiredFieldValidator();
        empValid.setMessage("Field value required.");

        NumberValidator numValid = new NumberValidator();
        numValid.setMessage("Invalid number entered.");

        cProv.setValidators(empValid);
        cMunicipality.setValidators(empValid);
        cWard.setValidators(empValid, numValid);

        if (cProv.validate() && cMunicipality.validate() && cWard.validate()) {
            try {
                Timestamp createdAt = new Timestamp(System.currentTimeMillis());

                try (Connection con = Service.ConnectDB()) {
                    PreparedStatement statement = con.prepareStatement(addWard, Statement.RETURN_GENERATED_KEYS);
                    statement.setInt(1, cMunId);
                    statement.setInt(2, Integer.parseInt(cWard.getText()));
                    statement.setString(3, cDesc.getText());
                    statement.setBoolean(4, true);
                    statement.setTimestamp(5, createdAt);

                    statement.executeUpdate();
                    ResultSet rs = statement.getGeneratedKeys();

                    if (rs.next()) {
                        int generatedKey = rs.getInt(1);
                        FontAwesomeIconView tick = new FontAwesomeIconView(FontAwesomeIcon.CHECK_CIRCLE);
                        tick.setStyle("-fx-fill: LIGHTGREEN");
                        tick.setGlyphSize(50);
                        Notifications notify = Notifications.create()
                                .title("Success")
                                .text("New ward successfully added.")
                                .graphic(tick)
                                .hideAfter(Duration.seconds(5))
                                .position(Pos.TOP_RIGHT);
                        notify.darkStyle();
                        notify.show();

                        tbWardList.getItems().clear();
                        loadWardData();

                        clearCreate();

                        firstPane.getChildren().clear();
                        FXMLLoader fFxmlLoader = new FXMLLoader(getClass().getResource("/microservice/view/ward/View.fxml"));
                        fFxmlLoader.setController(wc);
                        Pane fNewLoadedPane = fFxmlLoader.load();
                        firstPane.getChildren().add(fNewLoadedPane);

                        loadViewWard(generatedKey);

                    } else {
                        FontAwesomeIconView tick = new FontAwesomeIconView(FontAwesomeIcon.TIMES_CIRCLE);
                        tick.setStyle("-fx-fill: ORANGERED");
                        tick.setGlyphSize(50);
                        Notifications notify = Notifications.create()
                                .title("Failed")
                                .text("Error occured while adding ward!")
                                .graphic(tick)
                                .hideAfter(Duration.seconds(5))
                                .position(Pos.TOP_RIGHT);
                        notify.darkStyle();
                        notify.show();
                    }

                    con.commit();
                    con.close();
                }
            } catch (HeadlessException | SQLException | IOException ex) {
                FontAwesomeIconView tick = new FontAwesomeIconView(FontAwesomeIcon.TIMES_CIRCLE);
                tick.setStyle("-fx-fill: ORANGERED");
                tick.setGlyphSize(50);
                Notifications notify = Notifications.create()
                        .title("Failed")
                        .text("Error occured while adding ward!")
                        .graphic(tick)
                        .hideAfter(Duration.seconds(5))
                        .position(Pos.TOP_RIGHT);
                notify.darkStyle();
                notify.show();

                System.out.print(ex);
                Logger.getLogger(WardController.class.getName()).log(Level.SEVERE, null, ex);
                ex.printStackTrace();
            }
        }
    }

    @FXML
    void cancel(MouseEvent event) {
        try {
            firstPane.getChildren().clear();
            FXMLLoader fFxmlLoader = new FXMLLoader(getClass().getResource("/microservice/view/ward/Create.fxml"));
            fFxmlLoader.setController(wc);
            Pane fNewLoadedPane = fFxmlLoader.load();
            firstPane.getChildren().add(fNewLoadedPane);

            loadProvData();

        } catch (IOException ie) {
            System.out.print(ie);
            Logger.getLogger(WardController.class.getName()).log(Level.SEVERE, null, ie);
            ie.printStackTrace();
        }
    }

    @FXML
    void update(MouseEvent event) {
        String updateWard = "UPDATE WARD SET MUN_ID = ?, WARD = ?, DESCRIPTION = ?, CREATED_AT = ? WHERE ID = ?";

        RequiredFieldValidator empValid = new RequiredFieldValidator();
        empValid.setMessage("Field value required.");

        NumberValidator numValid = new NumberValidator();
        numValid.setMessage("Invalid number entered.");

        cProv.setValidators(empValid);
        cMunicipality.setValidators(empValid);
        cWard.setValidators(empValid, numValid);

        if (cProv.validate() && cMunicipality.validate() && cWard.validate()) {
            try {
                Timestamp createdAt = new Timestamp(System.currentTimeMillis());
                int rowsUpdated;
                try (Connection con = Service.ConnectDB()) {

                    PreparedStatement statement = con.prepareStatement(updateWard);
                    statement.setInt(1, cMunId);
                    statement.setInt(2, Integer.parseInt(cWard.getText()));
                    statement.setString(3, cDesc.getText());
                    statement.setTimestamp(4, createdAt);

                    statement.setLong(5, wardId);

                    rowsUpdated = statement.executeUpdate();

                    if (rowsUpdated > 0) {
                        FontAwesomeIconView tick = new FontAwesomeIconView(FontAwesomeIcon.CHECK_CIRCLE);
                        tick.setStyle("-fx-fill: LIGHTGREEN");
                        tick.setGlyphSize(50);
                        Notifications notify = Notifications.create()
                                .title("Success")
                                .text("Ward updated successfully.")
                                .graphic(tick)
                                .hideAfter(Duration.seconds(5))
                                .position(Pos.TOP_RIGHT);
                        notify.darkStyle();
                        notify.show();

                        tbWardList.getItems().clear();
                        loadWardData();

                        clearCreate();

                        firstPane.getChildren().clear();
                        FXMLLoader fFxmlLoader = new FXMLLoader(getClass().getResource("/microservice/view/ward/View.fxml"));
                        fFxmlLoader.setController(wc);
                        Pane fNewLoadedPane = fFxmlLoader.load();
                        firstPane.getChildren().add(fNewLoadedPane);

                        loadViewWard(wardId);

                    } else {
                        FontAwesomeIconView tick = new FontAwesomeIconView(FontAwesomeIcon.TIMES_CIRCLE);
                        tick.setStyle("-fx-fill: ORANGERED");
                        tick.setGlyphSize(50);
                        Notifications notify = Notifications.create()
                                .title("Failed")
                                .text("Error occured while updating ward!")
                                .graphic(tick)
                                .hideAfter(Duration.seconds(5))
                                .position(Pos.TOP_RIGHT);
                        notify.darkStyle();
                        notify.show();
                    }

                    con.commit();
                    con.close();
                }

            } catch (HeadlessException | SQLException | IOException ex) {
                FontAwesomeIconView tick = new FontAwesomeIconView(FontAwesomeIcon.TIMES_CIRCLE);
                tick.setStyle("-fx-fill: ORANGERED");
                tick.setGlyphSize(50);
                Notifications notify = Notifications.create()
                        .title("Failed")
                        .text("Error occured while updating ward!")
                        .graphic(tick)
                        .hideAfter(Duration.seconds(5))
                        .position(Pos.TOP_RIGHT);
                notify.darkStyle();
                notify.show();

                System.out.print(ex);
                Logger.getLogger(WardController.class.getName()).log(Level.SEVERE, null, ex);
                ex.printStackTrace();
            }
        }
    }

    public void paneSetting() {
        try {
            firstPane.getChildren().clear();
            FXMLLoader fFxmlLoader = new FXMLLoader(getClass().getResource("/microservice/view/ward/Create.fxml"));
            fFxmlLoader.setController(wc);
            Pane fNewLoadedPane = fFxmlLoader.load();
            firstPane.getChildren().add(fNewLoadedPane);

            loadProvData();
//            loadMunData();
            secondPane.getChildren().clear();
            FXMLLoader sFxmlLoader = new FXMLLoader(getClass().getResource("/microservice/view/ward/Data.fxml"));
            sFxmlLoader.setController(wc);
            Pane sNewLoadedPane = sFxmlLoader.load();
            secondPane.getChildren().add(sNewLoadedPane);

            loadWardData();

        } catch (IOException ie) {
            System.out.print(ie);
            Logger.getLogger(WardController.class.getName()).log(Level.SEVERE, null, ie);
            ie.printStackTrace();
        }
    }

    public void loadProvData() {
        cProv.setConverter(new StringConverter<Province>() {
            @Override
            public String toString(Province object) {
                return object.getProvince();
            }

            @Override
            public Province fromString(String string) {
                return null;
            }
        });

        ObservableList<Province> provData = FXCollections.observableArrayList();
        try {
            Connection con = Service.ConnectDB();

            String allProv = "SELECT * FROM PROVINCE WHERE STATUS = ?";

            PreparedStatement pst = con.prepareStatement(allProv);
            pst.setBoolean(1, true);

            ResultSet rs = pst.executeQuery();

            while (rs.next()) {
                provData.add(new Province(rs.getInt("ID"), rs.getString("PROVINCE"), rs.getString("DESCRIPTION"), rs.getBoolean("STATUS"), rs.getDate("CREATED_AT")));
            }
            cProv.getItems().addAll(provData);

        } catch (SQLException se) {
            System.out.println(se);
            Logger.getLogger(WardController.class.getName()).log(Level.SEVERE, null, se);
            se.printStackTrace();

        }

        cProv.valueProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                cProvId = newValue.getId();
                cMunicipality.getItems().clear();
                loadMunData();
            }
        });
    }

    public void loadMunData() {
        cMunicipality.setConverter(new StringConverter<Municipality>() {
            @Override
            public String toString(Municipality object) {
                return object.getMunicipality();
            }

            @Override
            public Municipality fromString(String string) {
                return null;
            }
        });

        ObservableList<Municipality> munData = FXCollections.observableArrayList();
        try {
            Connection con = Service.ConnectDB();

            String allMun = "SELECT MUNICIPALITY.ID, MUNICIPALITY.PROV_ID, PROVINCE.PROVINCE, MUNICIPALITY.MUNICIPALITY, MUNICIPALITY.DESCRIPTION, MUNICIPALITY.STATUS, MUNICIPALITY.CREATED_AT FROM MUNICIPALITY INNER JOIN PROVINCE ON PROVINCE.ID = MUNICIPALITY.PROV_ID WHERE MUNICIPALITY.PROV_ID = ? AND MUNICIPALITY.STATUS = ? ORDER BY PROVINCE.PROVINCE DESC";

            PreparedStatement pst = con.prepareStatement(allMun);
            pst.setInt(1, cProvId);
            pst.setBoolean(2, true);

            ResultSet rs = pst.executeQuery();

            while (rs.next()) {
                munData.add(new Municipality(rs.getInt("ID"), rs.getInt("PROV_ID"), rs.getString("PROVINCE"), rs.getString("MUNICIPALITY"), rs.getString("DESCRIPTION"), rs.getBoolean("STATUS"), rs.getDate("CREATED_AT")));
            }
            cMunicipality.getItems().addAll(munData);

        } catch (SQLException se) {
            System.out.println(se);
            Logger.getLogger(WardController.class.getName()).log(Level.SEVERE, null, se);
            se.printStackTrace();

        }

        cMunicipality.valueProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                cMunId = newValue.getId();
            }
        });
    }

    public void loadWardData() {
        ObservableList<Ward> wardData = FXCollections.observableArrayList();
        try {
            Connection con = Service.ConnectDB();

            String allWard = "SELECT WARD.ID, WARD.MUN_ID, MUNICIPALITY.MUNICIPALITY, PROVINCE.PROVINCE, WARD.WARD, WARD.DESCRIPTION, WARD.STATUS, WARD.CREATED_AT FROM WARD INNER JOIN MUNICIPALITY ON MUNICIPALITY.ID = WARD.MUN_ID INNER JOIN PROVINCE ON PROVINCE.ID = MUNICIPALITY.PROV_ID WHERE WARD.STATUS = ? ORDER BY PROVINCE.PROVINCE DESC";

            PreparedStatement pst = con.prepareStatement(allWard);
            pst.setBoolean(1, true);

            ResultSet rs = pst.executeQuery();

            while (rs.next()) {
                wardData.add(new Ward(rs.getInt("ID"), rs.getInt("MUN_ID"), rs.getString("PROVINCE"), rs.getString("MUNICIPALITY"), rs.getInt("WARD"), rs.getString("DESCRIPTION"), rs.getBoolean("STATUS"), rs.getDate("CREATED_AT")));
            }

            tbWardList.getItems().addAll(wardData);

        } catch (SQLException se) {
            System.out.println(se);
            Logger.getLogger(WardController.class.getName()).log(Level.SEVERE, null, se);
            se.printStackTrace();

        }

        tbId.setSortable(false);
        tbId.setCellValueFactory(column -> new ReadOnlyObjectWrapper<Number>(tbWardList.getItems().indexOf(column.getValue()) + 1));
        tbProvince.setCellValueFactory(new PropertyValueFactory<>("province"));
        tbMunicipality.setCellValueFactory(new PropertyValueFactory<>("municipality"));
        tbWard.setCellValueFactory(new PropertyValueFactory<>("ward"));
        tbAction.setCellFactory(param -> new TableCell<Ward, Void>() {
            private final Button viewButton = new Button();
            private final Button editButton = new Button();
            private final Button deleteButton = new Button();
            private final HBox pane = new HBox(viewButton, editButton, deleteButton);

            {
                HBox.setMargin(editButton, new Insets(0, 0, 0, 2));
                HBox.setMargin(deleteButton, new Insets(0, 0, 0, 2));
                FontAwesomeIconView eye = new FontAwesomeIconView(FontAwesomeIcon.EYE);
                eye.setStyle("-fx-fill: DODGERBLUE");
                eye.setGlyphSize(12);

                FontAwesomeIconView edit = new FontAwesomeIconView(FontAwesomeIcon.EDIT);
                edit.setStyle("-fx-fill: DARKGREEN");
                edit.setGlyphSize(12);

                FontAwesomeIconView delete = new FontAwesomeIconView(FontAwesomeIcon.TRASH);
                delete.setStyle("-fx-fill: ORANGERED");
                delete.setGlyphSize(12);

                viewButton.setGraphic(eye);
                viewButton.setTooltip(new Tooltip("View"));
                viewButton.getStyleClass().add("eye-btn");
                viewButton.setOnAction(event -> {
                    try {
                        Ward getWard = getTableView().getItems().get(getIndex());

                        firstPane.getChildren().clear();
                        FXMLLoader fFxmlLoader = new FXMLLoader(getClass().getResource("/microservice/view/ward/View.fxml"));
                        fFxmlLoader.setController(wc);
                        Pane fNewLoadedPane = fFxmlLoader.load();
                        firstPane.getChildren().add(fNewLoadedPane);

                        loadViewWard(getWard.getId());
                    } catch (IOException ex) {
                        System.out.println(ex);
                        Logger.getLogger(WardController.class.getName()).log(Level.SEVERE, null, ex);
                        ex.printStackTrace();
                    }
                });

                editButton.setGraphic(edit);
                editButton.setTooltip(new Tooltip("Edit"));
                editButton.getStyleClass().add("edit-btn");
                editButton.setOnAction(event -> {

                    try {
                        Ward getWard = getTableView().getItems().get(getIndex());

                        firstPane.getChildren().clear();
                        FXMLLoader fFxmlLoader = new FXMLLoader(getClass().getResource("/microservice/view/ward/Update.fxml"));
                        fFxmlLoader.setController(wc);
                        Pane fNewLoadedPane = fFxmlLoader.load();
                        firstPane.getChildren().add(fNewLoadedPane);

                        loadUpdateWard(getWard.getId());
                        wardId = getWard.getId();
                    } catch (IOException ex) {
                        System.out.println(ex);
                        Logger.getLogger(WardController.class.getName()).log(Level.SEVERE, null, ex);
                        ex.printStackTrace();
                    }
                });

                deleteButton.setGraphic(delete);
                deleteButton.setTooltip(new Tooltip("Delete"));
                deleteButton.getStyleClass().add("del-btn");
                deleteButton.setOnAction(event -> {
                    Ward getWard = getTableView().getItems().get(getIndex());
                    deleteWard(getWard.getId());
                });
            }

            @Override
            protected void updateItem(Void item,
                    boolean empty) {
                super.updateItem(item, empty);

                setGraphic(empty ? null : pane);
            }
        });
    }

    public void loadViewWard(int wId) {
        try {
            Connection con = Service.ConnectDB();

            String viewWard = "SELECT WARD.ID, WARD.MUN_ID, MUNICIPALITY.MUNICIPALITY, PROVINCE.PROVINCE, WARD.WARD, WARD.DESCRIPTION, WARD.STATUS, WARD.CREATED_AT FROM WARD INNER JOIN MUNICIPALITY ON MUNICIPALITY.ID = WARD.MUN_ID INNER JOIN PROVINCE ON PROVINCE.ID = MUNICIPALITY.PROV_ID WHERE WARD.ID = ?";

            PreparedStatement pst = con.prepareStatement(viewWard);
            pst.setInt(1, wId);

            ResultSet rs = pst.executeQuery();
            while (rs.next()) {

                vProvince.setText(rs.getString("PROVINCE"));
                vMunicipality.setText(rs.getString("MUNICIPALITY"));
                vWard.setText(String.valueOf(rs.getInt("WARD")));
                vDesc.setWrapText(true);
                vDesc.setTextAlignment(TextAlignment.JUSTIFY);
                vDesc.setText(rs.getString("DESCRIPTION"));

                vStatus.setText(String.valueOf(rs.getBoolean("STATUS")));

                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                Date cdate = rs.getTimestamp("CREATED_AT");
                vCreatedAt.setText(dateFormat.format(cdate));
            }
        } catch (SQLException se) {
            System.out.println(se);
            Logger.getLogger(WardController.class.getName()).log(Level.SEVERE, null, se);
            se.printStackTrace();
        }
    }

    public void loadUpdateWard(int wId) {
        loadProvData();
        try {
            Connection con = Service.ConnectDB();

            String getWard = "SELECT WARD.ID, WARD.MUN_ID, MUNICIPALITY.MUNICIPALITY, PROVINCE.PROVINCE, WARD.WARD, WARD.DESCRIPTION, WARD.STATUS, WARD.CREATED_AT FROM WARD INNER JOIN MUNICIPALITY ON MUNICIPALITY.ID = WARD.MUN_ID INNER JOIN PROVINCE ON PROVINCE.ID = MUNICIPALITY.PROV_ID WHERE WARD.ID = ?";

            PreparedStatement pst = con.prepareStatement(getWard);
            pst.setInt(1, wId);

            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                updateTitle.setText(rs.getString("WARD"));
                cWard.setText(String.valueOf(rs.getInt("WARD")));
                cDesc.setText(rs.getString("DESCRIPTION"));
            }

        } catch (SQLException se) {
            System.out.println(se);
            Logger.getLogger(WardController.class.getName()).log(Level.SEVERE, null, se);
            se.printStackTrace();
        }
    }

    public void deleteWard(int wId) {
        JFXDialog jfxDialog = new JFXDialog();
        JFXDialogLayout content = new JFXDialogLayout();
        VBox vBox = new VBox();
        vBox.setSpacing(10);
        HBox hBox = new HBox();
        Label label = new Label("Are you sure you want to delete!");
        hBox.getChildren().addAll(label);
        HBox hBox2 = new HBox();
        JFXButton jfxButton1 = new JFXButton("Cancel");
        jfxButton1.setStyle("-fx-background-color: #eee; -fx-text-fill: #333");
        JFXButton jfxButton2 = new JFXButton("Yes");
        jfxButton2.setStyle("-fx-background-color: red; -fx-text-fill: white");

        Insets buttonInset = new Insets(0, 0, 0, 10);
        hBox2.setMargin(jfxButton2, buttonInset);
        hBox2.getChildren().addAll(jfxButton1, jfxButton2);
        vBox.getChildren().addAll(hBox, hBox2);
        content.setBody(vBox);
        jfxDialog.setContent(content);
        jfxDialog.setDialogContainer(wardStackPane);
        jfxDialog.show();

        EventHandler<ActionEvent> cancelDel = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                jfxDialog.close();
            }
        };
        jfxButton1.setOnAction(cancelDel);

        EventHandler<ActionEvent> confirmDel = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
//                String delWard = "UPDATE WARD SET STATUS =? WHERE ID =?";
                String delWard = "DELETE FROM WARD WHERE ID =?";
                PreparedStatement pstDel;
                try {
                    try (Connection con = Service.ConnectDB()) {

                        pstDel = con.prepareStatement(delWard);
//                        pstDel.setBoolean(1, false);
                        pstDel.setInt(1, wId);

                        int rowsDeleted = pstDel.executeUpdate();

                        if (rowsDeleted > 0) {
                            FontAwesomeIconView tick = new FontAwesomeIconView(FontAwesomeIcon.CHECK_CIRCLE);
                            tick.setStyle("-fx-fill: LIGHTGREEN");
                            tick.setGlyphSize(50);
                            Notifications notify = Notifications.create()
                                    .title("Success")
                                    .text("Ward successfully removed.")
                                    .graphic(tick)
                                    .hideAfter(Duration.seconds(5))
                                    .position(Pos.TOP_RIGHT);
                            notify.darkStyle();
                            notify.show();

                        } else {
                            FontAwesomeIconView tick = new FontAwesomeIconView(FontAwesomeIcon.TIMES_CIRCLE);
                            tick.setStyle("-fx-fill: ORANGERED");
                            tick.setGlyphSize(50);
                            Notifications notify = Notifications.create()
                                    .title("Invalid")
                                    .text("Error occured while deleting ward.")
                                    .graphic(tick)
                                    .hideAfter(Duration.seconds(5))
                                    .position(Pos.TOP_RIGHT);
                            notify.darkStyle();
                            notify.show();
                        }

                        con.commit();
                        con.close();
                    }

                    tbWardList.getItems().clear();
                    loadWardData();

                } catch (HeadlessException | SQLException ex) {
                    FontAwesomeIconView tick = new FontAwesomeIconView(FontAwesomeIcon.TIMES_CIRCLE);
                    tick.setStyle("-fx-fill: ORANGERED");
                    tick.setGlyphSize(50);
                    Notifications notify = Notifications.create()
                            .title("Invalid")
                            .text("Error occured while deleting ward.")
                            .graphic(tick)
                            .hideAfter(Duration.seconds(5))
                            .position(Pos.TOP_RIGHT);
                    notify.darkStyle();
                    notify.show();
                    System.out.print(ex);
                    Logger.getLogger(WardController.class.getName()).log(Level.SEVERE, null, ex);
                    ex.printStackTrace();
                }

                jfxDialog.close();
            }
        };
        jfxButton2.setOnAction(confirmDel);

    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url,
            ResourceBundle rb) {
        // TODO
    }

    @FXML
    private void gotoDash(MouseEvent event) {
        try {
            contentPane.getChildren().clear();
            Pane newLoadedPane = FXMLLoader.load(getClass().getResource("/microservice/view/Dashboard.fxml"));

            contentPane.getChildren().add(newLoadedPane);
        } catch (IOException e) {
            System.out.print(e);
        }
    }

}
