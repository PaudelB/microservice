/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package microservice.model;

import java.util.Date;

/**
 *
 * @author Dell
 */
public class Margh {

    private int id;

    private int toleId;

    private String province;

    private String municipality;

    private int ward;

    private String tole;

    private String margh;

    private String description;

    private Boolean status;

    private Date createdAt;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getToleId() {
        return toleId;
    }

    public void setToleId(int toleId) {
        this.toleId = toleId;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getMunicipality() {
        return municipality;
    }

    public void setMunicipality(String municipality) {
        this.municipality = municipality;
    }

    public int getWard() {
        return ward;
    }

    public void setWard(int ward) {
        this.ward = ward;
    }

    public String getTole() {
        return tole;
    }

    public void setTole(String tole) {
        this.tole = tole;
    }

    public String getMargh() {
        return margh;
    }

    public void setMargh(String margh) {
        this.margh = margh;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Margh(int id,
            int toleId,
            String province,
            String municipality,
            int ward,
            String tole,
            String margh,
            String description,
            Boolean status,
            Date createdAt) {
        this.id = id;
        this.toleId = toleId;
        this.province = province;
        this.municipality = municipality;
        this.ward = ward;
        this.tole = tole;
        this.margh = margh;
        this.description = description;
        this.status = status;
        this.createdAt = createdAt;
    }
}
