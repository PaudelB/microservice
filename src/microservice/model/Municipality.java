/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package microservice.model;

import java.util.Date;

/**
 *
 * @author Dell
 */
public class Municipality {
    
    private int id;

    private int provId;
    
    private String province;
    
    private String municipality;

    private String description;

    private Boolean status;

    private Date createdAt;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public int getProvId() {
        return provId;
    }

    public void setProvId(int provId) {
        this.provId = provId;
    }
    
    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getMunicipality() {
        return municipality;
    }

    public void setMunicipality(String municipality) {
        this.municipality = municipality;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Municipality(int id,
            int provId,
            String province,
            String municipality,
            String description,
            Boolean status,
            Date createdAt) {
        this.id = id;
        this.provId = provId;
        this.province = province;
        this.municipality = municipality;
        this.description = description;
        this.status = status;
        this.createdAt = createdAt;
    }
}
