/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package microservice.model;

import java.util.Date;

/**
 *
 * @author Dell
 */
public class Province {

    private int id;

    private String province;

    private String description;

    private Boolean status;

    private Date createdAt;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Province(int id,
            String province,
            String description,
            Boolean status,
            Date createdAt) {
        this.id = id;
        this.province = province;
        this.description = description;
        this.status = status;
        this.createdAt = createdAt;
    }

}
