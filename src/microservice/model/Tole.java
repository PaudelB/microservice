/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package microservice.model;

import java.util.Date;

/**
 *
 * @author Dell
 */
public class Tole {

    private int id;

    private int wardId;

    private String province;

    private String municipality;

    private int ward;

    private String tole;

    private String description;

    private Boolean status;

    private Date createdAt;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getWardId() {
        return wardId;
    }

    public void setWardId(int wardId) {
        this.wardId = wardId;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getMunicipality() {
        return municipality;
    }

    public void setMunicipality(String municipality) {
        this.municipality = municipality;
    }

    public int getWard() {
        return ward;
    }

    public void setWard(int ward) {
        this.ward = ward;
    }

    public String getTole() {
        return tole;
    }

    public void setTole(String tole) {
        this.tole = tole;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Tole(int id,
            int wardId,
            String province,
            String municipality,
            int ward,
            String tole,
            String description,
            Boolean status,
            Date createdAt) {
        this.id = id;
        this.wardId = wardId;
        this.province = province;
        this.municipality = municipality;
        this.ward = ward;
        this.tole = tole;
        this.description = description;
        this.status = status;
        this.createdAt = createdAt;
    }

}
