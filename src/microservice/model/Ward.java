/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package microservice.model;

import java.util.Date;

/**
 *
 * @author Dell
 */
public class Ward {

    private int id;

    private int munId;

    private String province;

    private String municipality;
    
    private int ward;

    private String description;

    private Boolean status;

    private Date createdAt;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMunId() {
        return munId;
    }

    public void setMunId(int munId) {
        this.munId = munId;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getMunicipality() {
        return municipality;
    }

    public void setMunicipality(String municipality) {
        this.municipality = municipality;
    }
    
    public int getWard() {
        return ward;
    }

    public void setWard(int ward) {
        this.ward = ward;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Ward(int id,
            int munId,
            String province,
            String municipality,
            int ward,
            String description,
            Boolean status,
            Date createdAt) {
        this.id = id;
        this.munId = munId;
        this.province = province;
        this.municipality = municipality;
        this.ward = ward;
        this.description = description;
        this.status = status;
        this.createdAt = createdAt;
    }

}
