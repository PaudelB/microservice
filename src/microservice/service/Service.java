/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package microservice.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Bimal Paudel
 */
public class Service {
    
    public static Connection ConnectDB(){
        try {
//            String jdbcURL = "jdbc:derby://localhost:1527/presentation";
//            String username = "demo";
//            String password = "D3m@";
//            Connection connection = null;
//            Class.forName("org.apache.derby.jdbc.EmbeddedDriver");
//            connection = DriverManager.getConnection(jdbcURL, username, password);
//            return connection;
            String jdbcURL = "jdbc:derby:G:\\it_deurali\\presentation\\microservice\\database\\Presentation";
            String username = "demo";
            String password = "D3m@";
            Connection connection = null;
            Class.forName("org.apache.derby.jdbc.EmbeddedDriver");
            connection = DriverManager.getConnection(jdbcURL, username, password);
            return connection;
        } catch (SQLException | ClassNotFoundException ex) {
            System.out.print(ex);
            Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
            return null;
        }
    }
}
